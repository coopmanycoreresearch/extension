//  PROJECT     : MAPP
//  FILE NAME   : application_management.c
//  MODULE NAME : Application Management
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : applications, management, memory allocation
//-----------------------------------------------------------------------------
//  PURPOSE     : Manages the application requirements and memory allocation.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------
#ifdef MULTIPROCESSOR

#include "application_management.h"
#include "resources_management.h"
#include "mapping.h"
#include "memory_macros.h"
#include "communication.h"
#include "network_interface.h"
#ifdef RTL
#ifdef MULTIPROCESSOR
#include "repository.h"
#endif // MULTIPROCESSOR
#endif // RTL

// VARIABLES {
uint8_t ucLocalRunningTasks = 0;
uint16_t usRunningTasks;
uint32_t ulNumberOfApps;
uint32_t ulMaxLocalTasks;
uint32_t ulSizeNocBuffer;
uint32_t ulAppsToMap;
uint32_t* pulAppsType;
uint32_t* pulAppsAddresses;
uint32_t* pulRepository;
uint32_t* pulTargetProcessor;
char tname[ configMAX_TASK_NAME_LEN ] = "Task ";
TaskItem_t* pxTaskList;
TaskHandle_t xTaskManagerHandler;
ApplicationPackage_t *pxFirstApplication, *pxLastApplication;
// }

ApplicationPackage_t* pxApiAppAllocation()
{
    int j;

    ApplicationPackage_t* pxApplication
        = ( ApplicationPackage_t* )
            pvPortMalloc( sizeof( ApplicationPackage_t ) );
    pxApplication->pxTasks
        = ( TaskPackage_t* )
            pvPortMalloc( sizeof( TaskPackage_t ) * MAX_APPS_TASKS );
    pxApplication->pulTaskLocationBuffer
        = ( uint32_t* )
            pvPortMalloc( sizeof( uint32_t ) * MAX_APPS_TASKS );
    for ( j=0; j<MAX_APPS_TASKS; j++ ) {
        pxApplication->pxTasks[j].pxDependences
            = ( DependencePackage_t* ) pvPortMalloc
                (
                sizeof( DependencePackage_t )
                *
                MAX_TASKS_DEPENDENCES
                );
    }
    return pxApplication;
}

ApplicationPackage_t* pxApiAppSearch( uint32_t ulAppid )
{
    ApplicationPackage_t* pxApplication;
    pxApplication = pxFirstApplication;
    while (
        ( pxApplication->usID != ulAppid )
        &&
        ( pxApplication->pxNextApp != NULL )
        ) {
            pxApplication = pxApplication->pxNextApp;
    }

    return pxApplication;
}

void vApiAppSort( ApplicationPackage_t* pxApplication )
{
    if ( pxFirstApplication == NULL ) {
        pxFirstApplication = pxApplication;
    } else {
        if ( pxLastApplication == NULL ) {
            pxLastApplication = pxApplication;
            pxLastApplication->pxPreviousApp = pxFirstApplication;
            pxFirstApplication->pxNextApp = pxLastApplication;
        } else {
            pxApplication->pxPreviousApp = pxLastApplication;
            pxLastApplication->pxNextApp = pxApplication;
            pxLastApplication = pxApplication;
        }
    }
}

void vStartupConstructor(){
#ifdef RTL
        ulNumberOfApps = NUMBER_OF_APPS;
        pulAppsType = ( uint32_t* ) &appstype;
#ifdef HETEROGENEOUS
        pulTargetProcessor = ( uint32_t* ) &target_pe;
        pulAppsAddresses = ( uint32_t* ) &apps_addresses_m0;
        pulRepository = ( uint32_t* ) &repository_m0;
#else
#ifdef CORTEX_M0
        pulAppsAddresses = ( uint32_t* ) &apps_addresses_m0;
        pulRepository = ( uint32_t* ) &repository_m0;
#endif
#ifdef CORTEX_M3
        pulAppsAddresses = ( uint32_t* ) &apps_addresses_m3;
        pulRepository = ( uint32_t* ) &repository_m3;
#endif
#endif
#else
        ulNumberOfApps = ADDRESS_APPS;
        pulAppsType = ( uint32_t* ) APPS_TYPE;
        pulAppsAddresses = ( uint32_t* ) APPS_ADDR;
        pulRepository = ( uint32_t* ) REPO_ADDR;
#endif
}

#ifdef RTL
#ifdef HETEROGENEOUS
void vSetTargetPERepository( uint32_t ulTargetPE ){
    if ( ulTargetPE == M0_PE ) {
        pulAppsAddresses = ( uint32_t* ) &apps_addresses_m0;
        pulRepository = ( uint32_t* ) &repository_m0;
    } else {
        pulAppsAddresses = ( uint32_t* ) &apps_addresses_m3;
        pulRepository = ( uint32_t* ) &repository_m3;
    }
}
#endif // HETEROGENEOUS
#endif // RTL

void vApiFreeApp( ApplicationPackage_t* pxApplication )
{
    int j;
    ApplicationPackage_t* pxAppAux;

    if ( pxApplication == pxFirstApplication ) {
        pxFirstApplication = pxApplication->pxNextApp;
        pxFirstApplication->pxPreviousApp = NULL;
    } else {
        if ( pxApplication == pxLastApplication ) {
            pxLastApplication = pxApplication->pxPreviousApp;
            pxLastApplication->pxNextApp = NULL;
        } else {
            pxAppAux = pxApplication->pxPreviousApp;
            pxAppAux->pxNextApp = pxApplication->pxNextApp;
            pxAppAux = pxApplication->pxNextApp;
            pxAppAux->pxPreviousApp = pxApplication->pxPreviousApp;
        }
    }
    for ( j=0; j<MAX_APPS_TASKS; j++ ) {
        vPortFree( pxApplication->pxTasks[j].pxDependences );
    }
    vPortFree(pxApplication->pxTasks);
    vPortFree(pxApplication->pulTaskLocationBuffer);
    vPortFree(pxApplication);
}

void vApiTaskItemAllocation()
{
    uint16_t x, y;
    pxTaskList
        = ( TaskItem_t* )
            pvPortMalloc( sizeof( TaskItem_t ) * ulMaxLocalTasks );
    for ( x=0; x<ulMaxLocalTasks; x++ ) {
        pxTaskList[x].pucMappingRequested
            = ( uint8_t* )
                pvPortMalloc( sizeof( uint8_t ) * MAX_APPS_TASKS );
        pxTaskList[x].pulTaskLocationBuffer
            = ( uint32_t* )
                pvPortMalloc( sizeof( uint32_t ) * MAX_APPS_TASKS );
        pxTaskList[x].pucMsgRequested
            = ( uint8_t* )
                pvPortMalloc( sizeof( uint8_t ) * MAX_APPS_TASKS );
        pxTaskList[x].pucMsgTo
            = ( uint8_t* )
                pvPortMalloc( sizeof( uint8_t ) * ulSizeNocBuffer );
        pxTaskList[x].pucMsgLength
            = ( uint8_t* )
                pvPortMalloc( sizeof( uint8_t ) * ulSizeNocBuffer );
        pxTaskList[x].piMessageBuffer
            = ( int ** )
                pvPortMalloc( sizeof( int * ) * ulSizeNocBuffer );
        for ( y=0; y < ulSizeNocBuffer; y++) {
            pxTaskList[x].piMessageBuffer[y]
                = ( int * ) pvPortMalloc( sizeof( int ) * MSG_SIZE );
        }
    }
    __ASM volatile("nop");
}

void vAppTaskManager()
{
    uint8_t ucTask = 0;
    uint8_t ucWaitingFor = 0;
    TaskItem_t* pxTask;

    vEnterCritical();
    {
    for( ucTask = 0 ; ucTask < ulMaxLocalTasks ; ucTask++ ) {
        pxTask = &pxTaskList[ucTask];
        if ( pxTask->xTaskHandler != NULL ) {
            if ( eTaskGetState( pxTask->xTaskHandler ) == eSuspended ) {
                if( pxTask->ucWaitingFor == 0 ) {
                    if( pxTask->ucPendent < ( ulSizeNocBuffer - 2 ) ) {
                        vTaskResume( pxTask->xTaskHandler );
                    }
                } else {
                    ucWaitingFor = pxTask->ucWaitingFor - 1;
                    if ( pxTask->pucMappingRequested[ ucWaitingFor ] == 2
                            &&
                            pxTask->pulTaskLocationBuffer[ ucWaitingFor ]) {
                        if ( pxTask->pulTaskLocationBuffer[ ucWaitingFor ]
                                ==
                                pxTask->pulTaskLocationBuffer[ pxTask->ucTaskID - 1 ]) {
                            vApiDeliverer(
                                ucTask,
                                pxTask->ucWaitingFor,
                                1,
                                (
                                pxTask->pulTaskLocationBuffer[ ucWaitingFor ]
                                &
                                0x0000FFFF
                                ) );
                        } else {
                            pxTask->pucMappingRequested[ ucWaitingFor ] = 0;
                            pxPacket = pxApiGetServiceHeaderSlot();
                            pxPacket->ulHeader
                                = pxTask->pulTaskLocationBuffer[ ucWaitingFor ]
                                >>16;
                            pxPacket->ulService = NI_HANDLER_MESSAGE_REQUEST;
                            pxPacket->ulTaskID
                                = ( pxTask->ucTaskID << 16 )
                                |
                                (
                                pxTask->pulTaskLocationBuffer[ ucWaitingFor ]
                                &
                                0x0000FFFF
                                );
                            pxPacket->ulAppID = pxTask->ulAppID;
                            pxPacket->ulDependences
                                = ( pxTask->ulAppID<<8 )
                                |
                                ( pxTask->ucWaitingFor );
                            vApiSendPacket( pxPacket, NULL, 0 );
                        }
                    }
                }
            }
        }
    }
    }
    vExitCritical();
}

uint32_t uApiGetTaskID()
{
    // The handle of the currently running ( calling ) task on the kernel.
    TaskHandle_t xHandle = xTaskGetCurrentTaskHandle();
    uint16_t x = 0;
    for ( x = 0 ; x < ulMaxLocalTasks ; x++ ) {
        if ( pxTaskList[ x ].xTaskHandler == xHandle ) {
            return( x );
        }
    }
}

void vAppEndTask()
{
    uint32_t ulID;

    ulID = uApiGetTaskID();
    vTaskDelete( NULL );
    if ( pxTaskList[ulID].ucPendent == 0 ) {
#if ( DEBUG > 2 )
        printf("PE (%d) : %d : %s : Deleted Task %d\n",
                PROCESSOR_ID,
                xTaskGetTickCount(),
                __func__,
                ulID);
#endif
        vReportTaskDelete( ulID );
    }
    vPortFree( pxTaskList[ ulID ].piStackTask );
    ucLocalRunningTasks--;
}

void vApiHandleNewApp(
        uint32_t ulNewAppID ,
        uint32_t ulAddress ,
        uint32_t ulAppSize)
{
    uint32_t ulInitialTasks[ MAX_INITIAL_TASKS ];
    uint8_t ucTaskID;
    uint8_t i , j;
    ApplicationPackage_t* pxCurrentApp;
    TaskPackage_t* pxCurrentTask;

    if ( PROCESSOR_ID != ulGlobalMaster ) {
        pxCurrentApp = pxApiAppAllocation();
        pxCurrentApp->usID = ulNewAppID;
        vApiAppSort(pxCurrentApp);
        pxCurrentApp->ucTaskNum = ulApiReadNI();
        pxCurrentApp->ucTaskFinished = 0;
        pxCurrentApp->usSize = ulApiReadNI();
        for ( i = 0; i < MAX_INITIAL_TASKS; i++ ) {
            ulInitialTasks[ i ] = ulApiReadNI();
        }

        for ( i = 0; i < ( pxCurrentApp->ucTaskNum ); i++ ) {
            ucTaskID = ulApiReadNI();
            pxCurrentTask = &pxCurrentApp->pxTasks[ ucTaskID ];
            pxCurrentTask->usSize = ulApiReadNI();
            pxCurrentTask->usBss = ulApiReadNI();
            pxCurrentTask->ulInitialAddress = ulApiReadNI();
            pxCurrentTask->usProcessor = -1;
            pxCurrentTask->ucDependencesNumber = 0;
            for ( j = 0; j < ( MAX_INITIAL_TASKS + 1 ); j++ ) {
                pxCurrentTask->pxDependences[ j ].usTask = ulApiReadNI();
                pxCurrentTask->pxDependences[ j ].usFlits = ulApiReadNI();
                if (
                    pxCurrentTask->pxDependences[ j ].usTask
                    !=
                    ( uint16_t ) EMPTY
                   ) {
                        pxCurrentTask->ucDependencesNumber++;
                }
            }
            ulApiReadNI();
            ulApiReadNI();
        }
    } else {

        pxCurrentApp = pxApiAppSearch(ulNewAppID);
        pxCurrentApp->ucTaskNum = pulRepository[ ulAddress++ ];
        pxCurrentApp->ucTaskFinished = 0;
        pxCurrentApp->usSize = pulRepository[ ulAddress++ ];
        for ( i = 0; i < MAX_INITIAL_TASKS; i++ ) {
            ulInitialTasks[ i ] = pulRepository[ ulAddress++ ];
        }

        for ( i = 0; i < ( pxCurrentApp->ucTaskNum ); i++ ) {
            ucTaskID = pulRepository[ ulAddress++ ];
            pxCurrentTask = &pxCurrentApp->pxTasks[ ucTaskID ];
            pxCurrentTask->usSize = pulRepository[ ulAddress++ ];
            pxCurrentTask->usBss = pulRepository[ ulAddress++ ];
            pxCurrentTask->ulInitialAddress = pulRepository[ ulAddress++ ];
            pxCurrentTask->usProcessor = -1;
            pxCurrentTask->ucDependencesNumber = 0;
            for ( j = 0; j < ( MAX_INITIAL_TASKS + 1 ); j++ ) {
                pxCurrentTask->pxDependences[ j ].usTask
                    = pulRepository[ ulAddress++ ];
                pxCurrentTask->pxDependences[ j ].usFlits
                    = pulRepository[ ulAddress++ ];
                if (
                    pxCurrentTask->pxDependences[ j ].usTask
                    !=
                    ( uint16_t ) EMPTY
                    ) {
                        pxCurrentTask->ucDependencesNumber++;
                }
            }
            ulAddress += 2;
        }
    }
    vApiAllocateInitialTask( pxCurrentApp , ulInitialTasks );
#ifdef PREMAP
    for( i = 0; i < ( pxCurrentApp->ucTaskNum ); i++ ) {
        if( pxCurrentApp->pxTasks[ i ].usProcessor == (uint16_t) EMPTY ){
            vApiRequestMapping(
                ulNewAppID,
                pxCurrentApp->pxTasks[ ulInitialTasks[0] - 1 ].usProcessor,
                i + 1 );
        }
    }
    for( i = 0; i < ( pxCurrentApp->ucTaskNum ); i++ ) {
        vApiSendTask( i + 1, pxCurrentApp, pxCurrentApp->pxTasks[ i ].ucID );
        vApiSendTLBUpdate( i + 1, pxCurrentApp );
    }
#endif // PREMAP
}

void vApiHandleNewTask( uint32_t ulAppID, uint32_t ulAppTaskID )
{
    ApplicationPackage_t* pxApplication = pxApiAppSearch( ulAppID );
    vApiSendTask(
        ulAppTaskID,
        pxApplication,
        pxApplication->pulTaskLocationBuffer[ulAppTaskID-1]&0x0000FFFF
        );
}

void vApiHandleTaskTerminated(
        uint32_t ulAppID,
        uint32_t ulAppTaskID,
        uint32_t ulSenderProcessor,
        uint32_t ulSenderLocalTaskID)
{
    ApplicationPackage_t* pxApplication;

    pxApplication = pxApiAppSearch( ulAppID );
    ucProcessorUsedID[ ulSenderProcessor ][ ulSenderLocalTaskID ] = 0;
    pxApplication->pxTasks[ulAppTaskID].usProcessor = 0;
    vApiTaskReleased( ulSenderProcessor );
    pxApplication->ucTaskFinished++;
    if ( pxApplication->ucTaskFinished >= pxApplication->ucTaskNum ) {
        if ( PROCESSOR_ID == ulGlobalMaster ) {
            vApiAppFinished( pxApplication->usID );
        } else {
            pxClusterInfo[ 0 ].ulFreeResources += pxApplication->ucTaskNum;
            pxPacket = pxApiGetServiceHeaderSlot();
            pxPacket->ulHeader = ulApiRouterAddress( ulGlobalMaster );
            pxPacket->ulService = NI_HANDLER_APP_TERMINATED;
            pxPacket->ulTaskID = ulSenderLocalTaskID;
            pxPacket->ulAppID = pxApplication->usID;
            pxPacket->ulDependences = 0;
            vApiSendPacket( pxPacket , ( uint32_t ) NULL , 0 );
        }
    }
}

void vApiHandleTaskAllocation(
        uint32_t ulPayloadSize,
        uint32_t ulBSSSize,
        uint32_t ulDependences,
        uint32_t ulLocalTaskID,
        uint32_t ulAppTaskID)
{
    int i;
    TaskHandle_t xTaskToAllocateHandle;

    pxTaskList[ ulLocalTaskID ].piStackTask
                = ( int * ) pvPortMalloc( sizeof( int ) *
            ( ( ulPayloadSize + ulBSSSize ) - PACKAGE_HEADER ) );
    pxTaskList[ ulLocalTaskID ].ucTaskID = ( ulAppTaskID & 0x00FF );
    pxTaskList[ ulLocalTaskID ].ulAppID = ulAppTaskID >> 8;
    pxTaskList[ ulLocalTaskID ].ucDependences = ulDependences;
    pxTaskList[ ulLocalTaskID ].ucPendent = 0;

    for ( i = 0 ; i < ulPayloadSize - PACKAGE_HEADER ; i++ ) {
        pxTaskList[ ulLocalTaskID ].piStackTask[ i ] = ulApiReadNI();
        if (
                ( ( uint32_t ) pxTaskList[ ulLocalTaskID ].piStackTask[ i ] >> 16 )
                == 0xCCCC
           ) {
            pxTaskList[ ulLocalTaskID ].piStackTask[ i ]
                = ( pxTaskList[ ulLocalTaskID ].piStackTask[ i ] & 0x0000FFFF )
                + ( uint32_t ) pxTaskList[ ulLocalTaskID ].piStackTask;
        }
    }
    for ( ;
          i < ( ( ulPayloadSize + ulBSSSize ) - PACKAGE_HEADER );
          i++ ) {
        pxTaskList[ ulLocalTaskID ].piStackTask[ i ] = 0;
    }
    for ( i = 0 ; i < ulDependences ; i++ ) {
        pxTaskList[ ulLocalTaskID ].pucMappingRequested[ i ] = 0;
        pxTaskList[ ulLocalTaskID ].pucMsgRequested[ i ] = 0;
    }
    for ( i = 0 ; i < ulSizeNocBuffer ; i++ ) {
        pxTaskList[ ulLocalTaskID ].pucMsgTo[ i ] = 0;
        pxTaskList[ ulLocalTaskID ].pucMsgLength[ i ] = 0;
    }

    tname[6] = ( char ) ulLocalTaskID;

    xTaskCreate(
            pxTaskList[ ulLocalTaskID ].piStackTask ,
            tname ,
            configTASK_STACK_SIZE,
            NULL ,
            tskIDLE_PRIORITY + 2,
            &xTaskToAllocateHandle );
    pxTaskList[ ulLocalTaskID ].xTaskHandler = xTaskToAllocateHandle;

    if ( !ucLocalRunningTasks ) {
        vTaskResume( xTaskManagerHandler );
    }
    ucLocalRunningTasks++;
}

uint8_t ucApiHandleAppRequest( uint32_t ulGlobalAppID )
{
    uint32_t ulAppType ,
             ulNumberOfTasks ,
             ulAppDescriptorSize;
    uint8_t ucClusterSelected;
    ApplicationPackage_t* pxApplication;
    ulAppType = pulAppsType[ ulGlobalAppID ];
    ulNumberOfTasks = pulRepository[ pulAppsAddresses[ ulAppType ] ];
    ulAppDescriptorSize = 11 + ( TASK_HEADER * ulNumberOfTasks );
    ucClusterSelected = ulApiSearchCluster( ulNumberOfTasks );

    if ( ucClusterSelected == ( uint8_t ) EMPTY ) {
        return 0;
    } else {
        usRunningTasks += ulNumberOfTasks;
    }

    pxApplication = pxApiAppAllocation();
    vApiAppSort( pxApplication );
    pxApplication->usID = ulGlobalAppID;
    pxApplication->ucType = ulAppType;
    pxApplication->ucTaskNum = ulNumberOfTasks;
    pxApplication->ucCluster = ucClusterSelected;
    usMpsocResources -= ulNumberOfTasks;

    vApirReserveClusterResources( ucClusterSelected , ulNumberOfTasks );

    if ( ucClusterSelected == 0 ) {

        vApiHandleNewApp(
                ulGlobalAppID,
                pulAppsAddresses[ulAppType],
                ulAppDescriptorSize );

    } else {

        pxPacket = pxApiGetServiceHeaderSlot();
        pxPacket->ulHeader
            = ulApiRouterAddress( pxClusterInfo[ucClusterSelected].usMasterID );
        pxPacket->ulService = NI_HANDLER_NEW_APP;
        pxPacket->ulTaskID = 0;
        pxPacket->ulAppID = ulGlobalAppID;
        pxPacket->ulDependences = ulNumberOfTasks;

        vApiSendPacket(
                pxPacket,
                ( uint32_t ) &pulRepository[ pulAppsAddresses[ ulAppType ] ],
                ulAppDescriptorSize );
    }
    return 1;
}

void vApiAppFinished( uint32_t ulAppID )
{
    uint32_t i;
    ApplicationPackage_t* pxApplication;

    pxApplication = pxApiAppSearch( ulAppID );
    usMpsocResources += pxApplication->ucTaskNum;
    usRunningTasks = ( usRunningTasks - pxApplication->ucTaskNum );
    pxClusterInfo[ pxApplication->ucCluster ].ulFreeResources
        += pxApplication->ucTaskNum;
    vApiFreeApp( pxApplication );
    if ( ( ulAppsToMap < ulNumberOfApps ) && ( ulAppsToMap > 0 ) ) {
        if ( ucApiHandleAppRequest( ulAppsToMap ) ) {
            ulAppsToMap++;
        }
    } else {
        if ( usRunningTasks <= 0 ) {
#ifdef OVP
            for ( i = 1 ; i < ulNumberOfCpus ; i++ ) {
                pxPacket = pxApiGetServiceHeaderSlot();
                pxPacket->ulHeader = ulApiRouterAddress( i );
                pxPacket->ulService = NI_HANDLER_SLEEP;
                pxPacket->ulTaskID = 0;
                pxPacket->ulAppID = 0;
                pxPacket->ulDependences = 0;
                vApiSendPacket( pxPacket , ( uint32_t ) NULL , 0 );
            }
#endif // OVP
            vApiSleepPE();
        }
    }
}

void vApiGenerateTLB( ApplicationPackage_t* pxApplication )
{
     uint8_t j;

     for ( j = 0 ; j < pxApplication->ucTaskNum ; j++ ){
        if ( pxApplication->pxTasks[ j ].usProcessor != ( uint16_t ) EMPTY ) {

            pxApplication->pulTaskLocationBuffer[ j ]
                =
                ulApiRouterAddress( pxApplication->pxTasks[ j ].usProcessor ) << 16
                |
                pxApplication->pxTasks[ j ].ucID ;
        } else {
            pxApplication->pulTaskLocationBuffer[ j ] = 0;
        }
    }
}

void vApiSendTLBUpdate( uint32_t ulTaskID, ApplicationPackage_t* pxApplication )
{
    uint16_t j;

    vApiGenerateTLB( pxApplication );
    pxPacket = pxApiGetServiceHeaderSlot();

#ifndef PREMAP
    for ( j = 0; j < pxApplication->ucTaskNum ; j++ ) {
        if ( pxApplication->pxTasks[ j ].usProcessor != ( uint16_t ) EMPTY ) {
            pxPacket->ulHeader
                = ulApiRouterAddress( pxApplication->pxTasks[ j ].usProcessor );
            pxPacket->ulService = NI_HANDLER_UP_TLB;
            pxPacket->ulTaskID = ( pxApplication->pxTasks[ j ].ucID );
            pxPacket->ulAppID = pxApplication->usID;
            pxPacket->ulDependences = 0;
            vApiSendPacket(
                pxPacket,
                pxApplication->pulTaskLocationBuffer,
                pxApplication->ucTaskNum );
        }
    }
#else
    j = ulTaskID - 1 ;
    pxPacket->ulHeader = ulApiRouterAddress( pxApplication->pxTasks[ j ].usProcessor );
    pxPacket->ulService = NI_HANDLER_UP_TLB;
    pxPacket->ulTaskID = ( pxApplication->pxTasks[ j ].ucID );
    pxPacket->ulAppID = pxApplication->usID;
    pxPacket->ulDependences = 0;
    vApiSendPacket(
        pxPacket,
        pxApplication->pulTaskLocationBuffer,
        pxApplication->ucTaskNum );
#endif
}

void vApiReceiveTLBUpdate( uint16_t usAppID, uint16_t usTaskNum, uint16_t usLocalTaskID){
    uint8_t j;
    uint32_t ulData;
    ApplicationPackage_t* pxApplication;

    if ( pxClusterInfo[ 0 ].usMasterID == PROCESSOR_ID ) {
        pxApplication = pxApiAppSearch( usAppID );
    }
    for ( j = 0 ; j < usTaskNum ; j++ ) {
        ulData = ulApiReadNI();
        if ( pxClusterInfo[ 0 ].usMasterID == PROCESSOR_ID){
            if ( ulData ) {
                pxApplication->pxTasks[ j ].ucID = ulData & 0x0000FFFF;
                pxApplication->pxTasks[ j ].usProcessor
                    = ( ( ulData >> 24 )
                        + ( ulNumberOfCpusX *( ( ulData >> 16 ) & 0x00FF ) ) );
            }
            pxApplication->pulTaskLocationBuffer[ j ] = ulData;
        } else {
            pxTaskList[ usLocalTaskID ].pulTaskLocationBuffer[ j ] = ulData;
        }
    }
}

#endif // MULTIPROCESSOR
