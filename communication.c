//  PROJECT     : MAPP
//  FILE NAME   : communication.c
//  MODULE NAME : MPI-Like Communication
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : communication, noc, mpi, packet switching
//-----------------------------------------------------------------------------
//  PURPOSE     : Controls the system and application tasks communication.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifdef MULTIPROCESSOR

#include "communication.h"

#include "application_management.h"
#include "mapping.h"
#include "memory_macros.h"
#include "message.h"
#include "network_interface.h"
#include "router_include.h"
#include "resources_management.h"

ServiceHeaderSlot_t xServiceSlot1, xServiceSlot2;
ServiceHeader_t* pxPacket;

void vApiRouterInitialization( void )
{
#ifdef RTL
    uint32_t ulRouterID;

    ulRouterID = MemoryRead( READ_RTL_ROUTER_ID );

    ulRouterID = (
            ( ( ulRouterID % 256 ) * ulNumberOfCpusX )
            + ( ulRouterID >> 8 ) );
    PROCESSOR_ID = ulRouterID;
#endif // RTL
#ifdef OVP
    PROCESSOR_ID = MemoryRead( READ_INITIALIZE_ROUTER );
#endif // OVP
}

uint32_t ulApiRouterAddress( uint32_t ulRouter )
{
    uint32_t ulRouterAddress , ulPositionX , ulPositionY;
    ulPositionX = ( ulRouter % ulNumberOfCpusX ) << 8;
    ulPositionY = ulRouter / ulNumberOfCpusX;
    ulRouterAddress = ulPositionX | ulPositionY;
    return ulRouterAddress;
}

#ifdef OVP
#ifndef USE_DMA
void vApiWriteNI( uint32_t ulData )
{
    while(MemoryRead(READ_DMA_READY));
    MemoryWrite( WRITE_NI , ulData );
}
#endif // DMA
#endif // OVP

#ifdef RTL
void vApiWriteNI( uint32_t ulData , uint32_t ulNoCBuffer )
{
    MemoryWrite( ulNoCBuffer , ulData );
}
#endif // RTL

uint32_t ulApiReadNI()
{
#ifdef OVP
    MemoryWrite( WRITE_START_READ_NI , 1 );
    while ( MemoryRead( READ_NI_READY ) );
    return MemoryRead( READ_DATA_IN );
#endif // OVP

#ifdef RTL
    while ( MemoryRead( READ_RTL_NI_READY ) );
    return MemoryRead( READ_RTL_DATA_IN );
#endif // RTL
}

#ifdef USE_DMA
void vApiReadDMA( uint32_t ulInitialAddress , uint32_t ulMessageSize )
{
    MemoryWrite( WRITE_DMA_SIZE , ulMessageSize );
    MemoryWrite( WRITE_DMA_OP , WRITE );
    MemoryWrite( WRITE_DMA_ADDRESS , ulInitialAddress );
    MemoryWrite( WRITE_DMA_START , 1 );
    while ( MemoryRead( READ_DMA_READY ) );
}
#endif

void vApiInitServiceHeaderSlots()
{
    xServiceSlot1.ucStatus = ( uint8_t ) 1U;
    xServiceSlot2.ucStatus = ( uint8_t ) 1U;
}

ServiceHeader_t* pxApiGetServiceHeaderSlot()
{
    if ( xServiceSlot1.ucStatus ) {
        xServiceSlot1.ucStatus = ( uint8_t ) 0U;
        xServiceSlot2.ucStatus = ( uint8_t ) 1U;
        return &xServiceSlot1.xServiceHeader;
    } else {
        xServiceSlot1.ucStatus = ( uint8_t ) 1U;
        xServiceSlot2.ucStatus = ( uint8_t ) 0U;
        return &xServiceSlot2.xServiceHeader;
    }
}

void vApiSendMessage( Message* pxMessageToSend , uint32_t ulStackedR0 )
{
    uint16_t usProcessorReceiver;
    uint32_t ulLocalTaskID;
    TaskItem_t* pxCurrentTask;

    ulLocalTaskID = uApiGetTaskID();
    pxCurrentTask = &pxTaskList[ulLocalTaskID];
    pxCurrentTask->pucMsgLength[ pxCurrentTask->ucPendent ]
        = pxMessageToSend->length;

    vApiMemCpy( (uint32_t *) pxCurrentTask->piMessageBuffer[ pxCurrentTask->ucPendent ],
               (uint32_t *) pxMessageToSend->msg,
                pxMessageToSend->length );

    pxCurrentTask->pucMsgTo [ pxCurrentTask->ucPendent ] = ulStackedR0 + 1;
    pxCurrentTask->ucPendent++;

#ifndef PREMAP
    // Task is no mapped, request to master to map
    if ( pxCurrentTask->pulTaskLocationBuffer[ ( ulStackedR0 ) ] == 0 ) {
        if ( pxCurrentTask->pucMappingRequested[ ( ulStackedR0 ) ] == 0 ) {
            pxCurrentTask->pucMappingRequested[ ( ulStackedR0 ) ] = 1;
            pxPacket = pxApiGetServiceHeaderSlot();
            pxPacket->ulHeader = ulApiRouterAddress( pxClusterInfo[ 0 ].usMasterID );
            pxPacket->ulService = NI_HANDLER_REQ_MAP;
            pxPacket->ulTaskID = ulStackedR0 + 1;
            pxPacket->ulAppID = pxCurrentTask->ulAppID;
            pxPacket->ulDependences = PROCESSOR_ID;
            vApiSendPacket( pxPacket , ( uint32_t ) NULL , 0 );
        }
    } else { // Task is mapped, send message if was requested previously
        pxCurrentTask->pucMappingRequested[ ( ulStackedR0 ) ] = 0;
#endif
        if ( pxCurrentTask->pucMsgRequested[ ( ulStackedR0 ) ] == 1 ) {

            pxCurrentTask->pucMsgRequested[ ( ulStackedR0 ) ] = 0;
            usProcessorReceiver =
                (
                 pxCurrentTask->pulTaskLocationBuffer[ ( ulStackedR0 ) ] & 0xFF
                );
            vApiDeliverer(
                    ulLocalTaskID ,
                    ulStackedR0 + 1,
                    (
                    ( pxCurrentTask->pulTaskLocationBuffer[ ( ulStackedR0 ) ] >> 16 )
                    ==
                    ( pxCurrentTask->pulTaskLocationBuffer[ pxCurrentTask->ucTaskID - 1 ] >> 16 )
                    ) ,
                    usProcessorReceiver );
        }
#ifndef PREMAP
    }
#endif
    if ( pxCurrentTask->ucPendent >= ulSizeNocBuffer ) {
#if ( DEBUG > 2 )
        printf("PE (%d) : %d : %s : Suspended with %d on buffer\n",
                PROCESSOR_ID,
                xTaskGetTickCount(),
                __func__,
                pxCurrentTask->ucPendent);
#endif
        vTaskSuspend( NULL );
    }
}

void vApiMsgSented( uint32_t ulTaskID , uint32_t ulBufferID )
{
    uint16_t i , ucSum = 1;

    TaskItem_t* pxCurrentLocalTask = &pxTaskList[ulTaskID];

    if ( pxCurrentLocalTask->ucPendent > 1 ) {

        for ( i = ulBufferID ; i < pxCurrentLocalTask->ucPendent ; i++ ) {
            while ( pxCurrentLocalTask->pucMsgTo[ i + ucSum ] == 0
                    && ( i + ucSum ) < pxCurrentLocalTask->ucPendent ) {
                ucSum++;
            }
            if ( ( i + ucSum ) < pxCurrentLocalTask->ucPendent ) {

                pxCurrentLocalTask->pucMsgTo[ i ]
                    = pxCurrentLocalTask->pucMsgTo[ i + ucSum ];

                pxCurrentLocalTask->pucMsgLength[i]
                    = pxCurrentLocalTask->pucMsgLength[ i + ucSum ];

                vApiMemCpy( pxCurrentLocalTask->piMessageBuffer[i],
                    pxCurrentLocalTask->piMessageBuffer[ i + ucSum ],
                    ( pxCurrentLocalTask->pucMsgLength[ i + ucSum ] ) );

            } else {
                break;
            }
        }
    }
}

void vReportTaskDelete( uint8_t ucTask )
{
    pxTaskList[ ucTask ].xTaskHandler = NULL;
    pxPacket = pxApiGetServiceHeaderSlot();
    pxPacket->ulHeader
        = ulApiRouterAddress( pxClusterInfo[ 0 ].usMasterID );
    pxPacket->ulService = NI_HANDLER_TASK_TERMINATED;
    pxPacket->ulTaskID =
        (
            (
            pxTaskList[ ucTask ].ulAppID << 8
            |
            pxTaskList[ ucTask ].ucTaskID
            ) << 16
            | ucTask
        );
    pxPacket->ulAppID = pxTaskList[ ucTask ].ulAppID;
    pxPacket->ulDependences = PROCESSOR_ID;
    vApiSendPacket( pxPacket , ( uint32_t ) NULL , 0 );
}

void vApiSendTask( uint32_t ulTaskID , ApplicationPackage_t* pxApplication, uint16_t usID )
{
    uint8_t i;
    pxPacket = pxApiGetServiceHeaderSlot();

    if ( MY_CLUSTER == 0 ) {
        pxPacket->ulHeader
            = pxApplication->pulTaskLocationBuffer[ ( ulTaskID -1 ) ] >> 16;
#ifdef RTL
#ifdef HETEROGENEOUS
        vSetTargetPERepository( pulTargetProcessor[ pxApplication->pxTasks[ ( ulTaskID -1 ) ].usProcessor ] );
#endif // HETEROGENEOUS
#endif // RTL
        pxPacket->ulService = NI_HANDLER_TASK_ALLOCATION;
        pxPacket->ulTaskID
            = ( ( pxApplication->usID << 8 | ulTaskID ) << 16 | usID );
        pxPacket->ulAppID = GET_TASK_SIZE( pxApplication->usID, ulTaskID, BSS_SIZE );
        pxPacket->ulDependences = pxApplication->ucTaskNum;
        vApiSendPacket(
                pxPacket,
                // Address of task + HEADER
                ( uint32_t ) GET_TASK_ADDRESS( pxApplication->usID, ulTaskID ) ,
                GET_TASK_SIZE( pxApplication->usID, ulTaskID, BIN_SIZE ) );
    } else {
        // Update the ulTaskLocationBuffer list of addresses of PEs with our app tasks
        vApiGenerateTLB( pxApplication );
        pxPacket->ulHeader = ulApiRouterAddress( ulGlobalMaster );
        pxPacket->ulService = NI_HANDLER_UP_TLB;
        pxPacket->ulTaskID = usID;
        pxPacket->ulAppID = pxApplication->usID;
        pxPacket->ulDependences = 0;
        vApiSendPacket(
                pxPacket,
                pxApplication->pulTaskLocationBuffer,
                pxApplication->ucTaskNum );

        pxPacket->ulHeader      = ulApiRouterAddress( ulGlobalMaster );
        pxPacket->ulService     = NI_HANDLER_NEW_TASK;
        pxPacket->ulTaskID = ulTaskID;
        pxPacket->ulAppID       = pxApplication->usID;
        pxPacket->ulDependences = 0;
        vApiSendPacket( pxPacket, ( uint32_t ) NULL, 0 );
    }
}

void vApiSendPacket(
        ServiceHeader_t *pxPacket ,
        uint32_t ulInitialAddress ,
        uint32_t ulMessageSize )
{
    uint32_t x;
    uint32_t* pulData;
    vEnterCritical();
    {
    pxPacket->ulPayloadSize = ( PACK_FLITS - NI_HEADER ) + ulMessageSize;

#ifdef RTL
    pxPacket->ulHeader = DMA_LOW + ( pxPacket->ulHeader << 4 );

    while( MemoryRead( READ_RTL_NI_READY ) );

#ifdef DMNI

    MemoryWrite( WRITE_RTL_DMA_SIZE, PACK_FLITS );
    MemoryWrite( WRITE_RTL_DMA_ADDRESS, ( uint32_t ) pxPacket );
    if( ulMessageSize > 0 ) {
        MemoryWrite( WRITE_RTL_DMA_SIZE_2, ulMessageSize );
        MemoryWrite( WRITE_RTL_DMA_ADDRESS_2, ulInitialAddress );
    }

#else

        vApiWriteNI( pxPacket->ulHeader,      pxPacket->ulHeader );
        vApiWriteNI( pxPacket->ulPayloadSize, pxPacket->ulHeader );
        vApiWriteNI( pxPacket->ulService,     pxPacket->ulHeader );
        vApiWriteNI( pxPacket->ulTaskID,      pxPacket->ulHeader );
        vApiWriteNI( pxPacket->ulAppID,       pxPacket->ulHeader );
        vApiWriteNI( pxPacket->ulDependences, pxPacket->ulHeader );
        if ( ulMessageSize > 0 ) {
                pulData = ulInitialAddress;
                for (x = 0 ; x < ulMessageSize ; x++ , pulData++ ) {
                        vApiWriteNI( *pulData , pxPacket->ulHeader );
                }
        }

#endif

    MemoryWrite( WRITE_RTL_NI_WR_END , 0 );
    while( MemoryRead( READ_RTL_NI_READY ) );

#endif // RTL

#ifdef OVP
    while ( MemoryRead( READ_DMA_READY ) );
#ifdef USE_DMA
    MemoryWrite( WRITE_DMA_SIZE , PACK_FLITS );
    MemoryWrite( WRITE_DMA_ADDRESS , ( uint32_t ) pxPacket );
    if ( ulMessageSize > 0 ) {
        MemoryWrite( WRITE_DMA_SIZE_2 , ulMessageSize );
        MemoryWrite( WRITE_DMA_ADDRESS_2 , ulInitialAddress );
    }
    MemoryWrite( WRITE_DMA_OP , READ );
    MemoryWrite( WRITE_DMA_START , 1 );
#else
    vApiWriteNI( pxPacket->ulHeader );
    vApiWriteNI( pxPacket->ulPayloadSize );
    vApiWriteNI( pxPacket->ulService );
    vApiWriteNI( pxPacket->ulTaskID );
    vApiWriteNI( pxPacket->ulAppID );
    vApiWriteNI( pxPacket->ulDependences );
    if ( ulMessageSize > 0 ) {
        pulData = ulInitialAddress;
        for ( x = 0 ; x < ulMessageSize ; x++, pulData++ ) {
            // TODO is right to pass a pointer to a pointer in this case?
            vApiWriteNI( * pulData );
        }
    }
#endif // DMA
    while ( MemoryRead(READ_DMA_READY) );
#endif // OVP

    }
    vExitCritical();
    __ASM("nop");
}

void vApiDeliverer(
        uint8_t ucSenderTaskID,
        uint8_t ulReceiverID,
        uint8_t ucLocal,
        uint8_t ucReceiverTaskID)
{
    int iMsgIndex;
    uint8_t i;
    uint8_t ucMessageWasDelivered = 0;
    TaskItem_t* pxReceiverTask;
    TaskItem_t* pxSenderTask = &pxTaskList[ ucSenderTaskID ];

    if ( pxSenderTask->ucPendent > 0 ) {

        for ( i = 0 ; i < pxSenderTask->ucPendent ; i++ ) {
            if ( pxSenderTask->pucMsgTo[ i ] == ulReceiverID ) {

                ucMessageWasDelivered = 1;
                pxSenderTask->pucMsgTo[ i ] = 0;

                if ( ucLocal ) {

                    pxReceiverTask = &pxTaskList[ ucReceiverTaskID ];
                    pxReceiverTask->pxMsgToReceive->length
                        = pxSenderTask->pucMsgLength[ i ];

                    for ( iMsgIndex = 0 ;
                          iMsgIndex < pxReceiverTask->pxMsgToReceive->length ;
                          iMsgIndex ++
                        ) {

                        pxReceiverTask->pxMsgToReceive->msg[ iMsgIndex ]
                            = pxSenderTask->piMessageBuffer[ i ][ iMsgIndex];

                    }
                    pxReceiverTask->ucWaitingFor = 0;
                    pxReceiverTask->pucMappingRequested[ ulReceiverID - 1 ] = 0;

                } else {
                    pxPacket = pxApiGetServiceHeaderSlot();
                    pxPacket->ulHeader
                        = pxSenderTask->pulTaskLocationBuffer[ ( ulReceiverID - 1 ) ]
                        >> 16;
                    pxPacket->ulService = NI_HANDLER_MESSAGE_DELIVERY;
                    pxPacket->ulTaskID =
                        (
                         ( pxSenderTask->ucTaskID << 16 )
                         |
                         ( pxSenderTask->pulTaskLocationBuffer[ ulReceiverID-1 ]
                           &
                           0x0000FFFF
                         )
                        );
                    pxPacket->ulAppID = pxSenderTask->ulAppID;
                    pxPacket->ulDependences =
                        ( pxSenderTask->ulAppID << 8 )
                        |
                        ulReceiverID;

                    vApiSendPacket(
                        pxPacket ,
                        ( uint32_t ) pxSenderTask->piMessageBuffer[ i ],
                        pxSenderTask->pucMsgLength[ i ]
                        );
                }
                vApiMsgSented( ucSenderTaskID , i );
                pxSenderTask->ucPendent--;
                break;
            }
        }
    }

    if ( ucMessageWasDelivered ) {
        if ( eTaskGetState( pxSenderTask->xTaskHandler ) == eDeleted ) {
            if ( pxSenderTask->ucPendent == 0 ) {
                vReportTaskDelete( ucSenderTaskID );
            }
        }
    } else {
        pxSenderTask->pucMsgRequested[ ulReceiverID-1 ] = 1;
        if ( ucLocal ) {
            vTaskSuspend( NULL );
        }
    }
}

void vApiRecvMessage( Message* pxMessageToReceive , uint32_t ulStackedR0 )
{
    uint32_t ulID;
    TaskItem_t* pxCurrentTask;

    ulID = uApiGetTaskID();
    pxCurrentTask = &pxTaskList[ ulID ];
    pxCurrentTask->ucWaitingFor = ulStackedR0 + 1;
    pxCurrentTask->pxMsgToReceive = pxMessageToReceive;
    if ( pxCurrentTask->pulTaskLocationBuffer[ ( ulStackedR0 ) ] != 0 ) {
        if (
             ( pxCurrentTask->pulTaskLocationBuffer[ ( pxCurrentTask->ucTaskID -1 ) ] >> 16 )
             ==
             ( pxCurrentTask->pulTaskLocationBuffer[ ( ulStackedR0 ) ] >> 16 )
           ) {
                vApiDeliverer(
                    (
                     pxCurrentTask->pulTaskLocationBuffer[ ( ulStackedR0 ) ]
                     &
                     0x0000FFFF
                    ),
                    pxCurrentTask->ucTaskID,
                    1,
                    ulID );
        } else {
            pxPacket = pxApiGetServiceHeaderSlot();
            pxPacket->ulHeader
                = pxCurrentTask->pulTaskLocationBuffer[ ( ulStackedR0 ) ]
                >> 16;
            pxPacket->ulService = NI_HANDLER_MESSAGE_REQUEST;
            pxPacket->ulTaskID =
                (
                 ( ( pxCurrentTask->ucTaskID ) << 16 )
                 |
                 (
                  pxCurrentTask->pulTaskLocationBuffer[ ( ulStackedR0 ) ]
                  &
                  0x0000FFFF
                 )
                );
            pxPacket->ulAppID = pxCurrentTask->ulAppID;
            pxPacket->ulDependences =
                (
                 ( pxCurrentTask->ulAppID << 8 )
                 |
                 ( ulStackedR0 + 1 )
                );
            vApiSendPacket( pxPacket , ( uint32_t ) NULL , 0 );
            vTaskSuspend( NULL );
        }
    } else {
        pxCurrentTask->pucMappingRequested[ ( ulStackedR0 ) ] = 2 ;
        vTaskSuspend( NULL );
    }
}

void vApiHandleMessageDelivery( uint32_t ulPayloadSize, uint32_t ulNIPID )
{
    int i;
    pxTaskList[ ulNIPID ].pxMsgToReceive->length
        = ulPayloadSize;
    for ( i = 0 ; i < ulPayloadSize ; i++ ) {
        pxTaskList[ ulNIPID ].pxMsgToReceive->msg[ i ] = ulApiReadNI();
    }
    pxTaskList[ ulNIPID ].ucWaitingFor = 0;
    vTaskResume( pxTaskList[ ulNIPID ].xTaskHandler );
}

#endif // MULTIPROCESSOR
