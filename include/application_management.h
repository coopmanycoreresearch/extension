//  PROJECT     : MAPP
//  FILE NAME   : application_management.h
//  MODULE NAME : Application Management Library
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : applications, management, memory allocation
//-----------------------------------------------------------------------------
//  PURPOSE     : Manages the application requirements and memory allocation.
//              This library contains the application management functions and
//              structures to provide the multiprocessor system management.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifndef __APPLICATION_MANAGEMENT_H__
#define __APPLICATION_MANAGEMENT_H__

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "message.h"

// STRUCTS {
typedef struct {
    union{
        uint16_t usTask;
        uint16_t usProcessor;
    };
    uint16_t usFlits;
} DependencePackage_t;

typedef struct {
    uint8_t ucID;
    uint8_t ucDependencesNumber;
    uint16_t usProcessor;
    uint16_t usSize;
    uint16_t usBss;
    uint32_t ulInitialAddress;
    DependencePackage_t* pxDependences;
} TaskPackage_t;

typedef struct {
    struct ApplicationPackage_t* pxNextApp;
    struct ApplicationPackage_t* pxPreviousApp;
    uint8_t ucCluster;
    uint8_t ucTaskNum;
    uint8_t ucTaskFinished;
    uint8_t ucType;
    uint16_t usID;
    uint16_t usSize;
    uint32_t* pulTaskLocationBuffer;
    TaskPackage_t* pxTasks;
} ApplicationPackage_t;

typedef struct {
    int* piStackTask;
    int** piMessageBuffer;
    uint8_t ucTaskID;
    uint8_t ucWaitingFor;
    uint8_t ucDependences;
    uint8_t ucPendent;
    uint8_t ucReceived;
    uint8_t* pucMsgTo;
    uint8_t* pucMsgLength;
    uint8_t* pucMappingRequested;
    uint8_t* pucMsgRequested;
    uint32_t* pulTaskLocationBuffer;
    uint32_t ulAppID;
    TaskHandle_t xTaskHandler;
    Message* pxMsgToReceive;
} TaskItem_t;
//}

// VARIABLES {
extern TaskHandle_t xTaskManagerHandler;
extern uint8_t ucLocalRunningTasks;
extern uint16_t usRunningTasks;
extern uint32_t ulNumberOfApps;
extern uint32_t ulMaxLocalTasks;
extern uint32_t ulSizeNocBuffer;
extern uint32_t ulAppsToMap;
extern uint32_t* pulAppsType;
extern uint32_t* pulAppsAddresses;
extern uint32_t* pulRepository;
extern uint32_t* pulTargetProcessor;
extern TaskItem_t* pxTaskList;
extern ApplicationPackage_t *pxFirstApplication, *pxLastApplication;
// }

void vStartupConstructor();
#ifdef RTL
#ifdef HETEROGENEOUS
void vSetTargetPERepository( uint32_t ulTargetPE );
#endif // HETEROGENEOUS
#endif // RTL
void vApiHandleNewApp(
        uint32_t ulNewAppID ,
        uint32_t ulAddress ,
        uint32_t ulAppSize );
void vApiHandleTaskAllocation(
        uint32_t ulPayloadSize ,
        uint32_t ulBSSSize ,
        uint32_t ulServDependent5 ,
        uint32_t ulNIPID ,
        uint32_t ulNITASK_ID );
void vApiHandleNewTask( uint32_t ulAppID , uint32_t ulAppTaskID );
void vApiHandleTaskTerminated(
        uint32_t ulAppID,
        uint32_t ulAppTaskID,
        uint32_t ulSenderProcessor,
        uint32_t ulSenderLocalTaskID );
uint8_t ucApiHandleAppRequest( uint32_t ulGlobalAppID );
void vApiHandleReqMap(
        uint32_t ulAppID ,
        uint32_t ulSourcePE ,
        uint32_t ulAppTaskID );
void vApiAppFinished( uint32_t ulAppID );
void vAppReq( Message* pxMessageToReceive );
uint32_t uApiGetTaskID();
void vAppTaskManager();
void vApiTaskItemAllocation();
void vAppEndTask();
ApplicationPackage_t* pxApiAppAllocation();
ApplicationPackage_t* pxApiAppSearch( uint32_t appid );
void vApiAppSort( ApplicationPackage_t* pxApplication );
void vApiFreeApp( ApplicationPackage_t* pxApplication );
void vApiSendTLBUpdate( uint32_t ulTaskID, ApplicationPackage_t* pxApplication );
void vApiTaskItemAllocation();

#endif /* __APPLICATION_MANAGEMENT_H__ */
