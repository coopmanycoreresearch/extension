//  PROJECT     : MAPP
//  FILE NAME   : architecture_include.h
//  MODULE NAME : Processor Architecture Includes
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : modularity, processor architecture, multiprocessor
//-----------------------------------------------------------------------------
//  PURPOSE     : Includes the processor architecture libraries.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifndef __ARCHITECTURE_INCLUDE_H__
#define __ARCHITECTURE_INCLUDE_H__

// FREERTOS INCLUDES {
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
// }

#ifdef OVP

#ifdef CORTEX_M0
#include "ARMCM0.h"
#define NI_INTERRUPTION 11
#endif // CORTEX_M0

#ifdef CORTEX_M3
#include "ARMCM3.h"
#define NI_INTERRUPTION 14
#endif // CORTEX_M3

#ifdef CORTEX_M4
#include "ARMCM4.h"
#define NI_INTERRUPTION 14
#endif // CORTEX_M4

#ifdef CORTEX_M4F
#include "ARMCM4_FP.h"
#define NI_INTERRUPTION 14
#endif // CORTEX_M4F

#endif // OVP

#ifdef RTL

#ifdef CORTEX_M0
#include "CMSDK_driver.h"
#include "CMSDK_CM0.h"
#define NI_INTERRUPTION 11
#endif // CORTEX_M0

#ifdef CORTEX_M3
#include "CM3DS_MPS2_driver.h"
#include "CM3DS_MPS2.h"
#define NI_INTERRUPTION 14
#endif // CORTEX_M3

#include "uart_stdout.h"

#endif // RTL

int PROCESSOR_ID;
int MY_CLUSTER;

#endif /*__ARCHITECTURE_INCLUDE_H__*/
