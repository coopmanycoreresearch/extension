//  PROJECT     : MAPP
//  FILE NAME   : communication.h
//  MODULE NAME : MPI-Like Communication Library
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : communication, noc, mpi, packet switching
//-----------------------------------------------------------------------------
//  PURPOSE     : Controls the system and application tasks communication.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#include "stdint.h"
#include "message.h"
#include "application_management.h"

typedef struct {
    uint32_t ulHeader;
    uint32_t ulPayloadSize;
    uint32_t ulService;
    uint32_t ulTaskID;
    uint32_t ulAppID;
    uint32_t ulDependences;
    //Add new variables here ...
} ServiceHeader_t;

typedef struct {
    ServiceHeader_t xServiceHeader;
    uint8_t ucStatus;
} ServiceHeaderSlot_t;

extern ServiceHeaderSlot_t xServiceSlot1, xServiceSlot2;
extern ServiceHeader_t* pxPacket;

void vApiRouterInitialization( void );
uint32_t ulApiRouterAddress( uint32_t ulRouter );
uint32_t ulApiReadNI( void );
void vApiInitServiceHeaderSlots( void );
ServiceHeader_t* pxApiGetServiceHeaderSlot( void );
void vApiSendMessage( Message* pxMessageToSend , uint32_t ulStackedR0 );
void vApiRecvMessage( Message* pxMessageToReceive , uint32_t ulStackedR0 );
void vApiMsgSented( uint32_t ulProcessID , uint32_t ulBufferID );
void vApiHandleMessageDelivery( uint32_t ulPayloadSize , uint32_t ulNIPID );
void vReportTaskDelete( uint8_t ucTask );
void vApiSendTask(
        uint32_t ulTaskID,
        ApplicationPackage_t* pxApplication,
        uint16_t usID );
void vApiSendPacket(
        ServiceHeader_t *pxPacket ,
        uint32_t ulInitialAddress ,
        uint32_t ulMessageSize );
void vApiDeliverer(
        uint8_t ucSenderTaskID,
        uint8_t ulReceiverID,
        uint8_t ucLocal,
        uint8_t ucReceiverTaskID );


#ifdef OVP
#ifndef USE_DMA
void vApiWriteNI( uint32_t ulData );
#endif // DMA
#endif // OVP

#ifdef RTL
// TODO, what is 'i'?
void vApiWriteNI( uint32_t ulData , uint32_t i );
#endif // RTL

#ifdef USE_DMA
void vApiReadDMA( uint32_t ulInitialAddress , uint32_t ulMessageSize );
#endif
