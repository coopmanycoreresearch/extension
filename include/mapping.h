//  PROJECT     : MAPP
//  FILE NAME   : mapping.h
//  MODULE NAME : Application and Task Mapping Library
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : mapping, workload distribution, management
//-----------------------------------------------------------------------------
//  PURPOSE     : Have the algorithms and structures to define task allocation
//              and execution.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifndef __MAPPING_H__
#define __MAPPING_H__

#include "stdint.h"
#include "application_management.h"

// DEFINES {
#define EMPTY                 (-1 )
#define HOP_NUMBER            ( 2 )
#define MAX_TASKS_DEPENDENCES ( 10 )
#define MAX_APPS_TASKS        ( 14 )
#define NI_HEADER             ( 2 )
#define PACKAGE_HEADER        ( 4 )
#define PACK_FLITS            ( 6 )
#define TASK_HEADER           ( 26 )
#define MAX_INITIAL_TASKS     ( 9 )
// }

uint32_t ulApiSearchCluster( uint32_t ulTaskResources );
uint32_t ulApiInitialMapping();
void vApiRequestMapping(
        uint32_t ulAppID,
        uint32_t ulProcessorID,
        uint32_t ulTaskID );
void vApiAllocateInitialTask(
                ApplicationPackage_t* pxApplication,
                uint32_t *pulInitialTask );

#ifdef LECDN
uint32_t ulApiMapTask(
                ApplicationPackage_t* pxApplication,
                uint32_t task );
#endif

#ifdef NN
int ulApiMapTask( int source_PE );
#endif

#endif /* __MAPPING_H__ */
