//  PROJECT     : MAPP
//  FILE NAME   : memory_macros.h
//  MODULE NAME : Macros Macros Library
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : memory access, memory management
//-----------------------------------------------------------------------------
//  PURPOSE     : Have the macros and defined addresses to access memory data.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifndef __MEMORY_MACROS_H__
#define __MEMORY_MACROS_H__

#include "stdint.h"
#include "router_include.h"
#include "resources_management.h"

// MACROS {
#define MemoryRead( A )         ( * ( ( volatile uint32_t * ) ( A ) ) )
#define MemoryWrite( A , V )    ( * ( ( volatile uint32_t * ) ( A ) ) = ( V ) )
#define ADDRESS_APPS            ( * ( ( volatile uint32_t * ) APPS_NUM ) )
#define ADDRESS_CPUS            ( * ( ( volatile uint32_t * ) CPUS_NUM ) )
#define ADDRESS_CPUS_X          ( * ( ( volatile uint32_t * ) X_CPUS ) )
#define ADDRESS_CPUS_Y          ( * ( ( volatile uint32_t * ) Y_CPUS ) )
#define ADDRESS_CLUSTER_H       ( * ( ( volatile uint32_t * ) H_CLUSTER ) )
#define ADDRESS_CLUSTER_W       ( * ( ( volatile uint32_t * ) W_CLUSTER ) )
#define ADDRESS_CLUSTER_N       ( * ( ( volatile uint32_t * ) N_CLUSTER ) )
#define ADDRESS_MASTER          ( * ( ( volatile uint32_t * ) G_MASTER ) )
#define ADDRESS_LOCAL_TASKS     ( * ( ( volatile uint32_t * ) L_TASKS ) )
#define ADDRESS_NBUFFER         ( * ( ( volatile uint32_t * ) NBUFFER ) )
#define GET_TASK_ADDRESS( APP , TASK ) ( &pulRepository[ pulRepository[ pulAppsAddresses[ pulAppsType[ APP ] ] + 11 + ( ( TASK -1 ) * TASK_HEADER ) + 3 ] ])
#define GET_TASK_SIZE( APP , TASK, TYPE ) ( pulRepository[pulAppsAddresses[ pulAppsType[ APP ] ] + 11 + ( ( TASK -1 ) * TASK_HEADER ) + TYPE] )
#define BIN_SIZE 1
#define BSS_SIZE 2
// }

#endif /*__MEMORY_MACROS_H__*/
