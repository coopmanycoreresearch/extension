//  PROJECT     : MAPP
//  FILE NAME   : message.h
//  MODULE NAME : MPI Message Library
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : communication, noc, mpi, packet switching, NI interruption
//-----------------------------------------------------------------------------
//  PURPOSE     : Have the MPI message struct.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#define MSG_SIZE 128

/**
 * struct Message
 *
 * DESCRIPTION:
 * Used to handle messages inside the task.
 * This is not the same structure used in the kernels.
 *
 **/
typedef struct {
    int length;
    int msg[ MSG_SIZE ];
} Message;
#endif /* __MESSAGE_H__ */
