//  PROJECT     : MAPP
//  FILE NAME   : misc.h
//  MODULE NAME : General Pourpose Functions
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    :
//-----------------------------------------------------------------------------
//  PURPOSE     :
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifndef __MISC_H__
#define __MISC_H__

#include "stdint.h"

#define TRUE  ( 1 )
#define FALSE ( 0 )

void vEnterCritical();
void vExitCritical();

void vApiMemSet(
        uint32_t *dst,
        int iValue,
        uint32_t ulBytes );

void vApiMemCpy(
        uint32_t* pulDestination,
        uint32_t *pulSource,
        uint32_t xSize );

void vMiscPrintf();

#define memset vApiMemSet
#define memcpy vApiMemCpy

#endif /* __MISC_H__ */
