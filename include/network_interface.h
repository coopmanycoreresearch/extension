//  PROJECT     : MAPP
//  FILE NAME   : network_interface.h
//  MODULE NAME : Network Interface Library
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : communication, noc, mpi, packet switching, NI interruption
//-----------------------------------------------------------------------------
//  PURPOSE     : Have the NoC services defines.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifndef __NETWORK_INTERFACE_H__
#define __NETWORK_INTERFACE_H__

#define NI_HANDLER_MESSAGE_REQUEST  ( 10 )
#define NI_HANDLER_MESSAGE_DELIVERY ( 20 )
#define NI_HANDLER_TASK_ALLOCATION  ( 30 )
#define NI_HANDLER_REQ_MAP          ( 40 )
#define NI_HANDLER_TASK_TERMINATED  ( 50 )
#define NI_HANDLER_CALL_SCHEDULER   ( 60 )
#define NI_HANDLER_NEW_TASK         ( 70 )
#define NI_HANDLER_APP_TERMINATED   ( 80 )
#define NI_HANDLER_NEW_APP          ( 90 )
#define NI_HANDLER_UP_TLB           ( 100 )
#define NI_HANDLER_SLEEP            ( 110 )

#endif /*__NETWORK_INTERFACE_H__*/
