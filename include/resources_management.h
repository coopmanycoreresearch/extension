//  PROJECT     : MAPP
//  FILE NAME   : resources_management.h
//  MODULE NAME : Resources Management Library
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : resources management, memory management, resources allocation
//-----------------------------------------------------------------------------
//  PURPOSE     : Controls the resources management and memory allocation.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifndef __RESOURCES_MANAGEMENT_H__
#define __RESOURCES_MANAGEMENT_H__

#include "architecture_include.h"

// STRUCTS {
typedef struct {
    uint16_t usLeftBottom_x;
    uint16_t usLeftBottom_y;
    uint16_t usTopRight_x;
    uint16_t usTopRight_y;
    uint16_t usMasterID;
    uint32_t ulFreeResources;
} ClusterInfo_t;
//}

// VARIABLES {
extern uint8_t** ucProcessorUsedID;
extern int** piPEFreeTasks;
extern uint16_t usMpsocResources;
extern uint32_t ulNumberOfCpus;
extern uint32_t ulNumberOfCpusX;
extern uint32_t ulNumberOfCpusY;
extern uint32_t ulClusterHeight;
extern uint32_t ulClusterWidth;
extern uint32_t ulNumberOfClusters;
extern uint32_t ulGlobalMaster;
extern ClusterInfo_t* pxClusterInfo;
// }

void vInitProcessorTasks( void );
uint32_t ulApiGetProcessorTask( uint32_t ulCurrentProcessor );
void vApiTaskReleased( uint32_t ulCurrentProcessor );
void vApiTaskUsed( uint32_t ulCurrentProcessor );
void vApiGenerateSquareClusters();
void vApirReserveClusterResources(
        uint32_t ulCurrentCluster,
        uint32_t ulTaskResources );
void vApiSleepPE( void );

#endif /* __RESOURCES_MANAGEMENT_H__ */
