//  PROJECT     : MAPP
//  FILE NAME   : router_include.h
//  MODULE NAME : NoC Router Defines Library
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : NoC Addresses, NoC Router, NoC Services
//-----------------------------------------------------------------------------
//  PURPOSE     : Have the NoC routing service defines.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#ifndef __ROUTER_INCLUDE_H__
#define __ROUTER_INCLUDE_H__

/* DMA operations */
#define READ  ( 0 )
#define WRITE ( 1 )

/* End Sim Detection */
#define SIM_SUCCESS         ( 1 )
#define IRQ_FAULT           ( 2 )
#define IRQ_NMI             ( 3 )
#define MALLOC_FAILED       ( 4 )
#define STACK_OVERFLOW      ( 5 )
#define SYSCALL_ERROR       ( 6 )
#define NI_HANDLER_ERROR    ( 7 )

#define BOTTOM_LEFT         ( 1 )
#define BOTTOM_CENTER       ( 2 )
#define BOTTOM_RIGHT        ( 3 )
#define CENTER_LEFT         ( 4 )
#define CENTER_CENTER       ( 5 )
#define CENTER_RIGHT        ( 6 )
#define TOP_LEFT            ( 7 )
#define TOP_CENTER          ( 8 )
#define TOP_RIGHT           ( 9 )
#define TOP_BOTTOM          ( 10 )
#define LEFT_RIGHT          ( 11 )
#define LEFT_TOP_RIGHT      ( 12 )
#define TOP_RIGHT_BOTTOM    ( 13 )
#define LEFT_BOTTOM_RIGHT   ( 14 )
#define TOP_LEFT_BOTTOM     ( 15 )

#define EAST                ( 0 )
#define WEST                ( 1 )
#define NORTH               ( 2 )
#define SOUTH               ( 3 )
#define LOCAL               ( 4 )
#define NPORT               ( 5 )
#define CAM_BUFFER          ( 8 )

#define DATANOC_CHANNELS    ( 2 )
#define EAST0               ( 0 )
#define EAST1               ( 1 )
#define WEST0               ( 2 )
#define WEST1               ( 3 )
#define NORTH0              ( 4 )
#define NORTH1              ( 5 )
#define SOUTH0              ( 6 )
#define SOUTH1              ( 7 )
#define LOCAL0              ( 8 )
#define LOCAL1              ( 9 )
#define DATANOC_NPORT       ( 10 )
#define PACKET_SWITCHING_SR ( 0x7 )

/* BrNoC SERVICES */
#define START_APP_SERVICE               1
#define TARGET_UNREACHABLE_SERVICE      2
#define CLEAR_SERVICE                   3
#define BACKTRACK_SERVICE               4
#define SEARCHPATH_SERVICE              5
#define END_TASK_SERVICE                6
#define SET_SECURE_ZONE_SERVICE         7
#define PACKET_RESEND_SERVICE           8
#define WARD_SERVICE                    9
#define OPEN_SECURE_ZONE_SERVICE        10
#define SECURE_ZONE_CLOSED_SERVICE      11
#define SECURE_ZONE_OPENED_SERVICE      12
#define FREEZE_TASK_SERVICE             13
#define UNFREEZE_TASK_SERVICE           14
#define MASTER_CANDIDATE_SERVICE        15
#define TASK_ALLOCATED_SERVICE          16
#define INITIALIZE_SLAVE_SERVICE        17
#define INITIALIZE_CLUSTER_SERVICE      18
#define LOAN_PROCESSOR_REQUEST_SERVICE  19
#define LOAN_PROCESSOR_RELEASE_SERVICE  20
#define END_TASK_OTHER_CLUSTER_SERVICE  21
#define WAIT_KERNEL_SERVICE             22
#define SEND_KERNEL_SERVICE             23
#define WAIT_KERNEL_SERVICE_ACK         24
#define FAIL_KERNEL_SERVICE             25
#define NEW_APP_SERVICE                 26
#define NEW_APP_ACK_SERVICE             27
#define GMV_READY_SERVICE               28
#define SET_SZ_RECEIVED_SERVICE         29
#define SET_EXCESS_SZ_SERVICE           30
#define RCV_FREEZE_TASK_SERVICE         31

#define SDRAM                   (           0x20000000 )
#define EXRAM                   (           0x60000000 )
#define CPUS_NUM                (           0X20000010 )
#define X_CPUS                  (           0X20000020 )
#define Y_CPUS                  (           0X20000030 )
#define H_CLUSTER               (           0X20000040 )
#define W_CLUSTER               (           0X20000050 )
#define N_CLUSTER               (           0X20000060 )
#define G_MASTER                (           0X20000070 )
#define L_TASKS                 (           0X20000080 )
#define NBUFFER                 (           0X20000090 )
#define APPS_NUM                (           0X60000000 )
#define APPS_TYPE               (           0X60000040 )
#define APPS_ADDR               (           0X60000900 )
#define REPO_ADDR               (           0X60001000 )

/**
 *
 *  DMA AND NI CALLBACKS
 **/
#ifdef RTL
#define DMA_LOW                 (           0xA0000000 )
#else
#define DMA_LOW                 (           0x40000000 )
#endif
#define DMA_HIGH                ( DMA_LOW + 0x00000fff )
#define AHB_ADDR                ( DMA_LOW + 0x00200000 )

/* OVP CALL BACKS READ */
#define CB_READ_LOW             ( DMA_LOW + 0x00002000 )
#define CB_READ_HIGH            ( DMA_LOW + 0x000020FF )
#define READ_DMA_READY          ( CB_READ_LOW + 0x0020 )
#ifdef RTL
#define READ_RTL_NI_READY       ( DMA_LOW + 0x00800000 )
#define READ_RTL_DATA_IN        ( DMA_LOW + 0x00200000 )
#define READ_RTL_ROUTER_ID      ( DMA_LOW + 0x00100000 )
#else
#define READ_NI_READY           ( CB_READ_LOW + 0x0030 )
#define READ_DATA_IN            ( CB_READ_LOW + 0x0040 )
#endif
#define READ_TICK_COUNTER       ( CB_READ_LOW + 0x0050 )
#define READ_INITIALIZE_ROUTER  ( CB_READ_LOW + 0x0060 )

/* OVP CALL BACKS WRITE */
#ifdef RTL
#define WRITE_RTL_NI_WR_END     ( DMA_LOW + 0x00400000 )
#define WRITE_RTL_DMA_SIZE_2    ( DMA_LOW + 0x00000210 )
#define WRITE_RTL_DMA_ADDRESS_2 ( DMA_LOW + 0x00000220 )
#define WRITE_RTL_DMA_SIZE      ( DMA_LOW + 0x00000230 )
#define WRITE_RTL_DMA_ADDRESS   ( DMA_LOW + 0x00000240 )
#endif
#define CB_WRITE_LOW            ( DMA_LOW + 0x00002200 )
#define CB_WRITE_HIGH           ( DMA_LOW + 0x000022FF )
#define WRITE_DMA_SIZE          ( CB_WRITE_LOW + 0x010 )
#define WRITE_DMA_SIZE_2        ( CB_WRITE_LOW + 0x020 )
#define WRITE_DMA_ADDRESS       ( CB_WRITE_LOW + 0x030 )
#define WRITE_DMA_ADDRESS_2     ( CB_WRITE_LOW + 0x040 )
#define WRITE_DMA_OP            ( CB_WRITE_LOW + 0x050 )
#define WRITE_DMA_START         ( CB_WRITE_LOW + 0x060 )
#define WRITE_START_READ_NI     ( CB_WRITE_LOW + 0x070 )
#define WRITE_NI                ( CB_WRITE_LOW + 0x080 )
#define WRITE_END_SIM           ( CB_WRITE_LOW + 0x090 )
#define WRITE_PRINT_OUT         ( CB_WRITE_LOW + 0x0A0 )
#define WRITE_INITIALIZE_ROUTER ( CB_WRITE_LOW + 0x0B0 )

#define SCHEDULING_REPORT       ( DMA_LOW + 0x00000027 )
#define INTERRUPTION            ( DMA_LOW + 0x00000100 )
#define SCHEDULER               ( DMA_LOW + 0x00000400 )
#define IDLE                    ( DMA_LOW + 0x00000800 )

#define NOC_SIZE_X              ( DMA_LOW + 0x00000061 )
#define NOC_SIZE_Y              ( DMA_LOW + 0x00000062 )
#define MASTER_LOC              ( DMA_LOW + 0x00000063 )
#define CLUSTER_NUMBER          ( DMA_LOW + 0x00000064 )

#endif /* __ROUTER_INCLUDE_H__ */
