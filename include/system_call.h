//  PROJECT     : MAPP
//  FILE NAME   : system_call.h
//  MODULE NAME : System Call Library
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : SVC interruption, operating system, middleware, system abstraction
//-----------------------------------------------------------------------------
//  PURPOSE     : Handles the system calls to execute user applications and communication.
//-----------------------------------------------------------------------------
//  COMMENTS    : The lib contains system call macros and pre defined services.
//-----------------------------------------------------------------------------

#ifndef __SYSTEM_CALL_H__
#define __SYSTEM_CALL_H__

#define SC_EXIT            ( 1 )
#define SC_END_TASK        ( 2 )
#define SC_SEND_MSG        ( 3 )
#define SC_RECV_MSG        ( 4 )
#define SC_PRINTF          ( 5 )
#define SC_APP_REQ         ( 6 )
#define SC_RESUMER         ( 7 )

#define SYSCALL_EXIT(A,B,C,D)    asm volatile ("mov r0, %3 \n mov r1, %2 \n mov r2, %1 \n mov r3, %0 \n svc #1 ":: "r"(D),"r"(C),"r"(B),"r"(A));/**/
#define SYSCALL_DELETE(A,B,C,D)  asm volatile ("mov r0, %3 \n mov r1, %2 \n mov r2, %1 \n mov r3, %0 \n svc #2 ":: "r"(D),"r"(C),"r"(B),"r"(A));/**/
#define SYSCALL_SEND(A,B,C,D)    asm volatile ("mov r0, %3 \n mov r1, %2 \n mov r2, %1 \n mov r3, %0 \n svc #3 ":: "r"((unsigned int*)D),"r"(C),"r"(B),"r"(A));/**/
#define SYSCALL_RCV(A,B,C,D)     asm volatile ("mov r0, %3 \n mov r1, %2 \n mov r2, %1 \n mov r3, %0 \n svc #4 ":: "r"((unsigned int*)D),"r"(C),"r"(B),"r"(A));/**/
#define SYSCALL_PRINTF(A,B,C,D)  asm volatile ("mov r0, %3 \n mov r1, %2 \n mov r2, %1 \n mov r3, %0 \n svc #5 ":: "r"(D),"r"(C),"r"(B),"r"(A));/**/
#define SYSCALL_APP_REQ(A,B,C,D) asm volatile ("mov r0, %3 \n mov r1, %2 \n mov r2, %1 \n mov r3, %0 \n svc #6 ":: "r"(D),"r"(C),"r"(B),"r"(A));/**/

#endif /*__SYSTEM_CALL_H__*/
