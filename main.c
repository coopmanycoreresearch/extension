//  PROJECT     : MAPP
//  FILE NAME   : main.c
//  MODULE NAME : Main
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : main, startup, constructurs
//-----------------------------------------------------------------------------
//  PURPOSE     : Start the system execution.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

/**
 *
 * The MAIN task.
 *
 * Start after system boot (i.e. startup.c), initializes data
 * structures and start application mapping feeding the platform. After,
 * starts the scheduler which activate interruptions and system execution.
 *
 *
 **/

#include "mapping.h"
#include "communication.h"
#include "system_call.h"
#include "resources_management.h"
#include "application_management.h"
#include "memory_macros.h"
#ifndef MULTIPROCESSOR
#include "application.h"
#endif

void vApplicationMallocFailedHook ( void * pvParameters );
void vApplicationStackOverflowHook( void * pvParameters );
void vStartSimulation (uint32_t ulApplicationID );
static void prvApiTaskManager(void * pvParameters);

void vApplicationMallocFailedHook ( void * pvParameters )
{
#ifdef OVP
    MemoryWrite( WRITE_END_SIM , MALLOC_FAILED );
#else
    UartEndSimulation();
#endif // OVP
}

void vApplicationStackOverflowHook( void * pvParameters )
{
#ifdef OVP
    MemoryWrite( WRITE_END_SIM , STACK_OVERFLOW );
#else
    UartEndSimulation();
#endif // OVP
}

#ifdef MULTIPROCESSOR
void vStartSimulation (uint32_t ulApplicationID )
{
    if(!ucApiHandleAppRequest(ulApplicationID)) {
        ulAppsToMap=ulApplicationID;
    }
}

static void prvApiTaskManager(void * pvParameters)
{
    for (;;) {
        vAppTaskManager();
        if ( ucLocalRunningTasks ) {
            taskYIELD();
        } else {
            vTaskSuspend( xTaskManagerHandler );
        }
    }
}

#ifdef CORTEX_M0
static char pcStartupPrintSlave[] = "MAPP FreeRTOS Says Hello from Slave Cortex-M0 PE";
static char pcStartupPrintMaster[] = "MAPP FreeRTOS Says Hello from Master Cortex-M0 PE";
#endif

#ifdef CORTEX_M3
static char pcStartupPrintSlave[] = "MAPP FreeRTOS Says Hello from Slave Cortex-M3 PE";
static char pcStartupPrintMaster[] = "MAPP FreeRTOS Says Hello from Master Cortex-M3 PE";
#endif

#ifdef CORTEX_M4F
static char pcStartupPrintSlave[] = "MAPP FreeRTOS Says Hello from Slave Cortex-M4F PE";
static char pcStartupPrintMaster[] = "MAPP FreeRTOS Says Hello from Master Cortex-M4F PE";
#endif
static char pcManagerTaskName[] = "MANAGER";

static void vApiMultiprocessorStartup(){
    uint32_t x;

    vApiRouterInitialization();

    ulAppsToMap     = 0;
    ulNumberOfCpus=ADDRESS_CPUS;
    ulNumberOfCpusX=ADDRESS_CPUS_X;
    ulNumberOfCpusY=ADDRESS_CPUS_Y;
    ulClusterHeight=ADDRESS_CLUSTER_H;
    ulClusterWidth=ADDRESS_CLUSTER_W;
    ulNumberOfClusters=ADDRESS_CLUSTER_N;
    ulGlobalMaster=ADDRESS_MASTER;
    ulMaxLocalTasks=ADDRESS_LOCAL_TASKS;
    ulSizeNocBuffer=ADDRESS_NBUFFER;

    vApiInitServiceHeaderSlots();
    vApiGenerateSquareClusters();

    if (PROCESSOR_ID == ulGlobalMaster) {
        printf("%s %d!\n", ( char * ) pcStartupPrintMaster, PROCESSOR_ID);
        vStartupConstructor();
        for ( x = 0 ; x < ulNumberOfApps ; x++ ) {
            if ( ulAppsToMap < 1) {
                vStartSimulation( x );
            } else {
                x = ulNumberOfApps;
            }
        }
    } else {
        printf("%s %d!\n", ( char * ) pcStartupPrintSlave, PROCESSOR_ID);
        if (PROCESSOR_ID != pxClusterInfo[0].usMasterID){
            vApiTaskItemAllocation();
            xTaskCreate(
                &prvApiTaskManager,       /* Function that implements the task. */
                pcManagerTaskName,        /* Text name for the task. */
                configTASK_STACK_SIZE,    /* Stack size in words, not bytes. */
                NULL,                     /* Parameter passed into the task. */
                tskIDLE_PRIORITY + 1,       /* Priority at which the task is created. */
                &xTaskManagerHandler);    /* Used to pass out the created task's handle. */
        }
    }
}

#else

#ifdef CORTEX_M0
static char pcStartupPrint[] = "MAPP FreeRTOS Says Hello from Single Cortex-M0";
#endif

#ifdef CORTEX_M3
static char pcStartupPrint[] = "MAPP FreeRTOS Says Hello from Single Cortex-M3";
#endif

#ifdef CORTEX_M4F
static char pcStartupPrint[] = "MAPP FreeRTOS Says Hello from Single Cortex-M4F";
#endif
static char pcMainTaskName[] = "MainTask";

static void vApiSingleProcessorStartup(){
    printf("%s \n",( char * ) pcStartupPrint );
    xTaskCreate(
            &vTask,                /* Function that implements the task. */
            pcMainTaskName,        /* Text name for the task. */
            configTASK_STACK_SIZE, /* Stack size in words, not bytes. */
            NULL,                  /* Parameter passed into the task. */
            tskIDLE_PRIORITY + 1,  /* Priority at which the task is created. */
            NULL);                 /* Used to pass out the created task's handle. */
}

#endif // MULTIPROCESSOR

void main( void )
{
#ifdef RTL
    UartStdOutInit();
#endif //RTL

#ifdef MULTIPROCESSOR
    vApiMultiprocessorStartup();
#else
    vApiSingleProcessorStartup();
#endif // MULTIPROCESSOR

vTaskStartScheduler();

vApiSleepPE();

}
