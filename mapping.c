//  PROJECT     : MAPP
//  FILE NAME   : mapping.c
//  MODULE NAME : Application and Task Mapping
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : mapping, workload distribution, management
//-----------------------------------------------------------------------------
//  PURPOSE     : Have the algorithms to define task allocation and execution.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------
#ifdef MULTIPROCESSOR

#include "mapping.h"

#include "application_management.h"
#include "communication.h"
#include "memory_macros.h"
#include "message.h"
#include "network_interface.h"
#include "router_include.h"
#include "resources_management.h"


uint32_t ulApiSearchCluster( uint32_t ulTaskResources )
{
    uint32_t x;
    uint32_t ulCluster = ( uint32_t ) -1,
             ulFreestCluster = 0;

    if ( usMpsocResources < ulTaskResources ) {
        return ( uint32_t ) -1;
    }

    for ( x = 0; x < ulNumberOfClusters; x++ ) {

        if( x == 0 ) continue;

        if ( pxClusterInfo[ x ].ulFreeResources > ulFreestCluster ) {
            ulCluster = x;
            ulFreestCluster = pxClusterInfo[x ].ulFreeResources;
        }
    }

    if ( pxClusterInfo[ 0 ].ulFreeResources > ulFreestCluster ) {
        ulFreestCluster = pxClusterInfo[ 0 ].ulFreeResources;
        ulCluster = 0;
    }

    if ( ulFreestCluster < ulTaskResources ) {
        return ( uint32_t ) -1;
    } else {
        return ulCluster;
    }
}

uint32_t ulApiInitialMapping()
{
    int x , y ,
        xo , yo ,
        hops , proc ,
        current_hops ,
        max_free_procs , free_procs;

    int xi , xf , yi , yf;

    proc = -1;
    hops = HOP_NUMBER;
    max_free_procs = -1;
    vEnterCritical();
    {
    for ( xo = pxClusterInfo[0].usLeftBottom_x; xo <= pxClusterInfo[0].usTopRight_x; xo++ ) {
        for ( yo = pxClusterInfo[0].usLeftBottom_y; yo <= pxClusterInfo[0].usTopRight_y; yo++ ) {
            if ( piPEFreeTasks[ xo ][ yo ] > 0 ) {
                xi = xo - hops;
                xf = xo + hops;
                yi = yo - hops;
                yf = yo + hops;

                if ( xi < pxClusterInfo[0].usLeftBottom_x ) xi = pxClusterInfo[0].usLeftBottom_x;
                if ( yi < pxClusterInfo[0].usLeftBottom_y ) yi = pxClusterInfo[0].usLeftBottom_y;
                if ( xf > pxClusterInfo[0].usTopRight_x ) xf = pxClusterInfo[0].usTopRight_x;
                if ( yf > pxClusterInfo[0].usTopRight_y ) yf = pxClusterInfo[0].usTopRight_y;

                free_procs = 0;

                for ( x = xi ; x <= xf ; x++ ) {
                    for ( y = yi ; y <= yf ; y++ ) {
                        current_hops = abs( xo - x ) + abs( yo - y );
                        if ( (current_hops <= hops)
                                && ( piPEFreeTasks[ x ][ y ] > 0 ) ) {
                            free_procs += ( piPEFreeTasks[ x ][ y ] );
                        }
                    }
                }
                if ( free_procs > max_free_procs ) {
                    proc = xo + ( yo * ulNumberOfCpusX );
                    max_free_procs = free_procs;
                }
            }
        }
    }
    if ( proc != ( int ) -1 ) {
        vApiTaskUsed( proc );
    }
    }
    vExitCritical();
    return proc;
}

void vApiRequestMapping(
        uint32_t ulAppID,
        uint32_t ulProcessorID,
        uint32_t ulTaskID )
{

    ApplicationPackage_t* pxApplication = pxApiAppSearch( ulAppID );
    if( pxApplication->pxTasks[ ulTaskID -1 ].usProcessor == (uint16_t) EMPTY ) {

#ifdef LECDN
        pxApplication->pxTasks[ ulTaskID -1 ].usProcessor
            = ulApiMapTask( pxApplication , ( ulTaskID -1 ) );
#endif
#ifdef NN
        pxApplication->pxTasks[ ulTaskID -1 ].usProcessor
            = ulApiMapTask( ulProcessorID );
#endif
        pxApplication->pxTasks[ ulTaskID -1 ].ucID
            = ulApiGetProcessorTask(
                pxApplication->pxTasks[ ulTaskID -1 ].usProcessor );

        pxApplication->pulTaskLocationBuffer[ ( ulTaskID -1 ) ]
            = ( ( ulApiRouterAddress( pxApplication->pxTasks[ ulTaskID -1 ].usProcessor ) << 16 )
                    | pxApplication->pxTasks[ ulTaskID -1 ].ucID );

#ifndef PREMAP
        vApiSendTask( ulTaskID, pxApplication, pxApplication->pxTasks[ ulTaskID -1 ].ucID);
        vApiSendTLBUpdate( ulTaskID, pxApplication );
#endif
    }
}

void vApiAllocateInitialTask(
        ApplicationPackage_t* pxApplication,
        uint32_t *pulInitialTask )
{
    int i;
    uint32_t ulTaskID = 0;
    for ( i = 0; i < pxApplication->ucTaskNum; i++ ) {
        if ( pulInitialTask[ i ] != ( uint32_t ) EMPTY ) {
            ulTaskID = pulInitialTask[ i ] - 1;
            pxApplication->pxTasks[ ulTaskID ].usProcessor
                = ulApiInitialMapping();
            pxApplication->pxTasks[ ulTaskID ].ucID
                = ulApiGetProcessorTask( pxApplication->pxTasks[ ulTaskID ].usProcessor );
            pxApplication->pulTaskLocationBuffer[ ulTaskID ]
                = ( ulApiRouterAddress( pxApplication->pxTasks[ ulTaskID ].usProcessor ) <<16
                    |
                    pxApplication->pxTasks[ ulTaskID ].ucID );

#ifndef PREMAP
            vApiSendTask(
                pulInitialTask[ i ] ,
                pxApplication ,
                pxApplication->pxTasks[ ulTaskID ].ucID );
            vApiSendTLBUpdate(
                pulInitialTask[ i ] ,
                pxApplication );
#endif
        } else {
            break;
        }
    }
}

//TODO refactor both ulApiMapTask
#ifdef LECDN
uint32_t ulApiMapTask( ApplicationPackage_t* pxApplication, uint32_t task )
{
    uint32_t i ,
             x , y , xo , yo ,
             proc , mhop;

    int xi , xf , yi , yf ,
        left , right , top , bottom ,
        end , bb ;

    uint32_t min_hops , hops;

    DependencePackage_t current_dep;
    DependencePackage_t tasks[ MAX_APPS_TASKS ];

    int mapped_deps = 0;
    vEnterCritical();
    {
        for ( i = 0 ;
              i < pxApplication->pxTasks[ task ].ucDependencesNumber;
              i++ ) {

            tasks[ i ].usProcessor = 0;
            current_dep = pxApplication->pxTasks[ task ].pxDependences[ i ];

            if ( pxApplication->pxTasks[ current_dep.usTask -1 ].usProcessor
                    != ( uint16_t ) -1 ) {

                tasks[ mapped_deps ].usProcessor
                    = pxApplication->pxTasks[ current_dep.usTask -1 ].usProcessor;

                tasks[ mapped_deps ].usFlits = current_dep.usFlits;
                mapped_deps++;
            }
        }

        proc = -1;
        min_hops = -1;

        if ( mapped_deps == 1 ) {

            hops = 0;

            xo = tasks[ 0 ].usProcessor % ulNumberOfCpusX;
            yo = tasks[ 0 ].usProcessor / ulNumberOfCpusY;
            xi = xo - 1 ;
            xf = xo + 1 ;
            yi = yo - 1 ;
            yf = yo + 1 ;

            if ( xi < pxClusterInfo[0].usLeftBottom_x ) xi = pxClusterInfo[0].usLeftBottom_x;
            if ( yi < pxClusterInfo[0].usLeftBottom_y ) yi = pxClusterInfo[0].usLeftBottom_y;
            if ( xf > pxClusterInfo[0].usTopRight_x ) xf = pxClusterInfo[0].usTopRight_x;
            if ( yf > pxClusterInfo[0].usTopRight_y ) yf = pxClusterInfo[0].usTopRight_y;

            do {
                for ( x = xi ; x <= xf ; x++ ) {
                    for ( y = yi ; y <= yf ; y++ ) {
                        if ( piPEFreeTasks[ x ][ y ] > 0 ) {
                            if ( ( abs( xo - x ) + abs( yo - y ) ) == hops ) {
                                proc = x + ( y * ulNumberOfCpusX );
                            }
                        }
                    }
                }

                if ( proc == ( uint32_t ) -1 ) {
                    hops++;
                    if ( xi > pxClusterInfo[0].usLeftBottom_x ) xi -- ;
                    if ( yi > pxClusterInfo[0].usLeftBottom_y ) yi -- ;
                    if ( xf < pxClusterInfo[0].usTopRight_x ) xf++;
                    if ( yf < pxClusterInfo[0].usTopRight_y ) yf++;
                }

            } while ( hops <= (abs( pxClusterInfo[0].usTopRight_x - pxClusterInfo[0].usLeftBottom_x )
                        + abs( pxClusterInfo[0].usTopRight_y - pxClusterInfo[0].usLeftBottom_y ))
                    && ( proc == ( uint32_t ) -1 ) );
        } else {
            xi = tasks[ 0 ].usProcessor % ulNumberOfCpusX;
            xf = tasks[ 0 ].usProcessor % ulNumberOfCpusX;
            yi = tasks[ 0 ].usProcessor / ulNumberOfCpusY;
            yf = tasks[ 0 ].usProcessor / ulNumberOfCpusY;

            for ( i = 1 ; i < mapped_deps ; i++ ) {
                if ( ( tasks[ i ].usProcessor % ulNumberOfCpusX ) < xi )
                    xi = tasks[ i ].usProcessor % ulNumberOfCpusX;
                if ( ( tasks[ i ].usProcessor % ulNumberOfCpusX ) > xf )
                    xf = tasks[ i ].usProcessor % ulNumberOfCpusX;
                if ( ( tasks[ i ].usProcessor / ulNumberOfCpusY ) < yi )
                    yi = tasks[ i ].usProcessor / ulNumberOfCpusY;
                if ( ( tasks[ i ].usProcessor / ulNumberOfCpusY ) > yf )
                    yf = tasks[ i ].usProcessor / ulNumberOfCpusY;
            }

            xi = xi - 1 ;
            xf = xf + 1 ;
            yi = yi - 1 ;
            yf = yf + 1 ;

            if ( xi < pxClusterInfo[0].usLeftBottom_x ) xi = pxClusterInfo[0].usLeftBottom_x;
            if ( yi < pxClusterInfo[0].usLeftBottom_y ) yi = pxClusterInfo[0].usLeftBottom_y;
            if ( xf > pxClusterInfo[0].usTopRight_x ) xf = pxClusterInfo[0].usTopRight_x;
            if ( yf > pxClusterInfo[0].usTopRight_y ) yf = pxClusterInfo[0].usTopRight_y;

            end = 0;
            bb = 0;
            left = -1;
            right = -1;
            top = -1;
            bottom = -1;
            do {
                for ( x = xi ; x <= xf ; x++ )
                    for ( y = yi ; y <= yf ; y++ ) {
                        if ( bb == 0
                                || ( bb == 1
                                    && ( ( x == left
                                            && y != ( uint32_t ) -1 )
                                        || ( x == right
                                            && y != ( uint32_t ) -1 )
                                        || ( x != ( uint32_t ) -1
                                            && y == bottom )
                                        || ( x != ( uint32_t ) -1
                                            && y == top ) ) ) ) {
                            if ( piPEFreeTasks[ x ][ y ] > 0 ) {
                                hops = 0;
                                for ( i = 0 ; i < mapped_deps ; i++ ) {
                                    mhop = 0;
                                    xo = tasks[ i ].usProcessor % ulNumberOfCpusX;
                                    yo = tasks[ i ].usProcessor / ulNumberOfCpusY;
                                    mhop = abs( xo - x ) + abs( yo - y );
                                    hops = hops + mhop * tasks[ i ].usFlits;
                                    /*proc_free_slots = piPEFreeTasks[x][y];*/
                                }
                                if ( hops < min_hops ) {
                                    proc = x + ( y * ulNumberOfCpusX );
                                    min_hops = hops;
                                }
                            }
                        }
                    }
                if ( proc == ( uint32_t ) -1 ) {
                    end = 0;
                    bb = 1;
                    if ( xi > pxClusterInfo[0].usLeftBottom_x ) {
                        xi -- ;
                        left = xi;
                    } else {
                        end++;
                        left = -1;
                    }
                    if ( yi > pxClusterInfo[0].usLeftBottom_y ) {
                        yi -- ;
                        bottom = yi;
                    } else {
                        end++;
                        bottom = -1;
                    }
                    if ( xf < pxClusterInfo[0].usTopRight_x ) {
                        xf++;
                        right = xf;
                    } else {
                        end++;
                        right = -1;
                    }
                    if ( yf < pxClusterInfo[0].usTopRight_y ) {
                        yf++;
                        top = yf;
                    } else {
                        end++;
                        top = -1;
                    }
                }
            } while ( end != 4 && proc == ( uint32_t ) -1 );
        }
        if ( proc != ( uint32_t ) -1 ) {
            vApiTaskUsed( proc );
        }
    }
    vExitCritical();
    return proc;
}
#endif

#ifdef NN
int ulApiMapTask( int source_PE )
{
    int i , x , y , xo , yo , proc;
    int xi , xf , yi , yf;
    unsigned long int min_hops , hops;
    // Store the number of already mapped dependences of the required task.
    int mapped_deps = 0;
    vEnterCritical();
    {
        proc = -1;
        min_hops = 0xFFFFFFFF;
        hops = 0;
        xo = source_PE % ulNumberOfCpusX;
        yo = source_PE / ulNumberOfCpusY;
        xi = xo -1;
        xf = xo + 1;
        yi = yo -1;
        yf = yo + 1;

        if ( xi < pxClusterInfo[0].usLeftBottom_x ) xi = pxClusterInfo[0].usLeftBottom_x;
        if ( yi < pxClusterInfo[0].usLeftBottom_y ) yi = pxClusterInfo[0].usLeftBottom_y;
        if ( xf > pxClusterInfo[0].usTopRight_x ) xf = pxClusterInfo[0].usTopRight_x;
        if ( yf > pxClusterInfo[0].usTopRight_y ) yf = pxClusterInfo[0].usTopRight_y;

        do {

            for ( x = xi ; x <= xf ; x++ ) {
                for ( y = yi ; y <= yf ; y++ ) {
                    if ( piPEFreeTasks[ x ][ y ] > 0 ) {
                        if ( ( abs( xo - x ) + abs( yo - y ) ) == hops ) {
                            proc = x + ( y * ulNumberOfCpusX );
                        }
                    }
                }
            }

            if ( proc == -1 ) {
                hops++;
                if ( xi > pxClusterInfo[0].usLeftBottom_x ) xi -- ;
                if ( yi > pxClusterInfo[0].usLeftBottom_y ) yi -- ;
                if ( xf < pxClusterInfo[0].usTopRight_x ) xf++;
                if ( yf < pxClusterInfo[0].usTopRight_y ) yf++;
            }

        } while ( hops <= ( abs( pxClusterInfo[0].usTopRight_x - pxClusterInfo[0].usLeftBottom_x )
                    + abs( pxClusterInfo[0].usTopRight_y - pxClusterInfo[0].usLeftBottom_y ) )
                && ( proc == -1 ) );

        if ( proc != ( int ) -1 ) {
            vApiTaskUsed( proc );
        }
    }
    vExitCritical();
    return proc;
}
#endif

#endif // MULTIPROCESSOR
