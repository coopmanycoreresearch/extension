//  PROJECT     : MAPP
//  FILE NAME   : misc.c
//  MODULE NAME : General Pourpose Functions
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    :
//-----------------------------------------------------------------------------
//  PURPOSE     :
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------
#ifdef MULTIPROCESSOR

#include "misc.h"
#include "architecture_include.h"
#include "task.h"

void vEnterCritical()
{
    NVIC_DisableIRQ( NI_INTERRUPTION );
    taskENTER_CRITICAL();
}

void vExitCritical()
{
    taskEXIT_CRITICAL();
    NVIC_EnableIRQ( NI_INTERRUPTION );
}

void vApiMemCpy(
        uint32_t *pulDestination,
        uint32_t *pulSource,
        uint32_t xSize )
{
    while ( xSize -- ) {
        pulDestination[ xSize ] = pulSource[ xSize ];
    }
}

void vApiMemSet(
        uint32_t *dst,
        int iValue,
        uint32_t ulBytes )
{

    unsigned char *Dst = ( unsigned char* ) dst;

    while( ( int ) ulBytes-- > 0) {
      *Dst++ = ( unsigned char ) iValue;
    }
}

#if ( DEBUG > 1 )
void vMiscPrintf( void * pvMessageToPrint , uint32_t ulStackedR0 )
{
    printf( "PE (%d): ", PROCESSOR_ID );
    switch ( ulStackedR0 ) {
        case 0:
            {
                printf( "%s \n" , ( char * ) pvMessageToPrint );
                break;
            }

        case 1:
            {
                printf( "%d \n" , ( int ) pvMessageToPrint );
                break;
            }
        default: break;
    }
}
#endif // DEBUG
#endif // MULTIPROCESSOR
