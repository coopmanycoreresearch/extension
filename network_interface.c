//  PROJECT     : MAPP
//  FILE NAME   : network_interface.c
//  MODULE NAME : Network Interface
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : communication, noc, mpi, packet switching, NI interruption
//-----------------------------------------------------------------------------
//  PURPOSE     : Handles the NoC interruptions, system and task communication.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#include "network_interface.h"
#include "memory_macros.h"
#include "resources_management.h"
#include "application_management.h"
#include "communication.h"
#include "mapping.h"

/**
 *
 * FIX_FLITS
 *
 * 0:   address
 * 1:   payload size
 * 2:   service number
 * 3-5: service dependent
 *
 **/

//Handles the NoC Incomming data
void vNetworkInterfaceHandler( void )
{
    uint32_t NIPID, NITASK_ID, i;
    uint32_t FIX_FLITS[ NI_HEADER + PACKAGE_HEADER ];

    vEnterCritical();
    {

#ifdef MULTIPROCESSOR
// Reads packet target, size and Service flits together
#ifdef USE_DMA
    vApiReadDMA( FIX_FLITS , ( NI_HEADER + PACKAGE_HEADER ) );
#else
    i = 0;
    while ( i < ( NI_HEADER + PACKAGE_HEADER ) ) {
        FIX_FLITS[ i ] = ulApiReadNI();
        i++;
    }
#endif

    NIPID = ( FIX_FLITS[ 3 ] & 0x0000FFFF );
    NITASK_ID = ( ( FIX_FLITS[ 3 ] ) >> 16 );

    switch ( FIX_FLITS[ 2 ] ) {
        case NI_HANDLER_TASK_ALLOCATION:
            {
                vApiHandleTaskAllocation(
                        FIX_FLITS[ 1 ],
                        FIX_FLITS[ 4 ],
                        FIX_FLITS[ 5 ],
                        NIPID,
                        NITASK_ID);
                break ;
            }

        case NI_HANDLER_MESSAGE_DELIVERY:
            {
                vApiHandleMessageDelivery(
                    ( FIX_FLITS[ 1 ] - PACKAGE_HEADER ),
                    NIPID);
                break;
            }

        case NI_HANDLER_SLEEP:
            {
                vApiSleepPE();
                break;
            }

        case NI_HANDLER_UP_TLB:
            {
                vApiReceiveTLBUpdate(
                    FIX_FLITS[ 4 ],
                    ( FIX_FLITS[ 1 ] - PACKAGE_HEADER ),
                    FIX_FLITS[ 3 ] );
                break;
            }

        case NI_HANDLER_REQ_MAP:
            {
#ifndef PREMAP
                vApiRequestMapping(
                    FIX_FLITS[ 4 ],
                    FIX_FLITS[ 5 ],
                    NIPID );
#endif
                break;
            }

        case NI_HANDLER_TASK_TERMINATED:
            {
                vApiHandleTaskTerminated(
                        FIX_FLITS[ 4 ],
                        ( NITASK_ID & 0x00FF ) - 1,
                        FIX_FLITS[ 5 ],
                        NIPID );
                break;
            }

        case NI_HANDLER_NEW_TASK:
            {
                vApiHandleNewTask( FIX_FLITS[ 4 ] , NIPID );
                break;
            }

        case NI_HANDLER_NEW_APP:
            {
                vApiHandleNewApp(
                        FIX_FLITS[ 4 ],
                        ( uint32_t ) NULL,
                        ( FIX_FLITS[ 1 ] - ( PACKAGE_HEADER ) ) );
                break;
            }

        case NI_HANDLER_APP_TERMINATED:
            {
                vApiAppFinished( FIX_FLITS[ 4 ] );
                break;
            }

        case NI_HANDLER_MESSAGE_REQUEST:
            {
                vApiDeliverer( NIPID , NITASK_ID , 0 , 0 );
                break;
            }

        default:
            {
#if ( DEBUG > 2 )
                i=0;
                while ( i < ( NI_HEADER + PACKAGE_HEADER ) ) {
                    printf("PE (%d): NI%08x\n", PROCESSOR_ID, FIX_FLITS[ i ]);
                    i++;
                }
#endif // DEBUG
#ifdef OVP
                MemoryWrite( WRITE_END_SIM , NI_HANDLER_ERROR );
#else
                UartEndSimulation();
#endif // OVP
                break;
            }
    }
    NVIC_ClearPendingIRQ( NI_INTERRUPTION );
#endif // MULTIPROCESSOR
    }
    vExitCritical();
}

