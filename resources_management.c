//  PROJECT     : MAPP
//  FILE NAME   : resources_management.c
//  MODULE NAME : Resources Management
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : resources management, memory management, resources allocation
//-----------------------------------------------------------------------------
//  PURPOSE     : Controls the resources management and memory allocation.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------
#ifdef MULTIPROCESSOR

#include "resources_management.h"

#include "application_management.h"
#include "mapping.h"
#include "memory_macros.h"
#include "task.h"

uint8_t** ucProcessorUsedID;
int** piPEFreeTasks;
uint16_t usMpsocResources;
uint32_t ulNumberOfCpus;
uint32_t ulNumberOfCpusX;
uint32_t ulNumberOfCpusY;
uint32_t ulClusterHeight;
uint32_t ulClusterWidth;
uint32_t ulNumberOfClusters;
uint32_t ulGlobalMaster;
ClusterInfo_t* pxClusterInfo;

void vInitProcessorTasks( void )
{
    uint16_t x , y;
    piPEFreeTasks
        = ( int** ) pvPortMalloc ( sizeof( int* ) * ulNumberOfCpusX );
    for ( x = 0 ; x < ulNumberOfCpusX ; x++ ) {
        piPEFreeTasks[ x ]
            = ( int* ) pvPortMalloc ( sizeof( int ) * ulNumberOfCpusY );
        for ( y = 0 ; y < ulNumberOfCpusY ; y++ ) {
            piPEFreeTasks[ x ][ y ] = ulMaxLocalTasks;
        }
    }

    ucProcessorUsedID
        = ( uint8_t** ) pvPortMalloc ( sizeof( uint8_t* ) * ulNumberOfCpus );
    for ( x = 0 ; x < ulNumberOfCpus ; x++ ) {
        ucProcessorUsedID[ x ]
            = ( uint8_t* ) pvPortMalloc ( sizeof( uint8_t ) * ulMaxLocalTasks );
    }

    usMpsocResources = (
            ( ulNumberOfCpus - ulNumberOfClusters )
            * ulMaxLocalTasks
            );
}

uint32_t ulApiGetProcessorTask( uint32_t ulCurrentProcessor )
{
    uint32_t x , y;
    y = 0;
    for ( x = 0 ; x < ulMaxLocalTasks ; x++ ) {
        if ( ucProcessorUsedID[ ulCurrentProcessor ][ x ] == 0 ) {
            y = x;
            break;
        }
    }
    ucProcessorUsedID[ ulCurrentProcessor ][ y ] = 1;
    return( y );
}

void vApiTaskReleased( uint32_t ulCurrentProcessor )
{
    uint32_t ulProcessorX , ulProcessorY;
    ulProcessorX = ulCurrentProcessor % ulNumberOfCpusX;
    ulProcessorY = ulCurrentProcessor / ulNumberOfCpusX;
    piPEFreeTasks[ ulProcessorX ][ ulProcessorY ]++;
    pxClusterInfo[0].ulFreeResources++;
}

void vApiTaskUsed( uint32_t ulCurrentProcessor )
{
    uint32_t ulProcessorX , ulProcessorY;
    ulProcessorX = ulCurrentProcessor % ulNumberOfCpusX;
    ulProcessorY = ulCurrentProcessor / ulNumberOfCpusX;
    piPEFreeTasks[ ulProcessorX ][ ulProcessorY ]--;
    pxClusterInfo[0].ulFreeResources--;
}

void vApiGenerateSquareClusters()
{
    uint16_t usClustersOnX;
    uint16_t usCurrentClusterX , usCurrentClusterY;
    uint16_t usCurrentPositionX , usCurrentPositionY;
    uint32_t ulClusters;
    uint16_t i;

    usCurrentPositionX = ( PROCESSOR_ID % ulNumberOfCpusX );
    usCurrentPositionY = ( PROCESSOR_ID / ulNumberOfCpusX );
    usClustersOnX = ( ulNumberOfCpusX / ulClusterWidth );
    usCurrentClusterX = ( usCurrentPositionX / ulClusterWidth );
    usCurrentClusterY = ( usCurrentPositionY / ulClusterHeight );
    MY_CLUSTER = (usCurrentClusterX + (usCurrentClusterY * usClustersOnX));

    if ( PROCESSOR_ID == ulGlobalMaster ) {
        ulClusters = ulNumberOfClusters;
    } else {
        ulClusters = 1;
    }

    pxClusterInfo = ( ClusterInfo_t* ) pvPortMalloc ( sizeof( ClusterInfo_t ) * ulClusters );
    for ( i = 0 ; i < ulClusters ; i++ ) {

        pxClusterInfo[ i ].usLeftBottom_x = ( usCurrentClusterX * ulClusterWidth );
        pxClusterInfo[ i ].usLeftBottom_y = ( usCurrentClusterY * ulClusterHeight );
        pxClusterInfo[ i ].usTopRight_x = ( ulClusterWidth - 1 ) +
                            ( usCurrentClusterX * ulClusterWidth );
        pxClusterInfo[ i ].usTopRight_y = ( ulClusterHeight - 1 ) +
                            ( usCurrentClusterY * ulClusterHeight );
        pxClusterInfo[ i ].usMasterID = ( usCurrentClusterX * ulClusterWidth ) +
                            ( usCurrentClusterY * ulClusterHeight * ulNumberOfCpusX );
        pxClusterInfo[ i ].ulFreeResources = ulMaxLocalTasks *
                            ( ( ulClusterHeight * ulClusterWidth ) - 1 );
        if ( PROCESSOR_ID == pxClusterInfo[i].usMasterID ) {
            vInitProcessorTasks();
            piPEFreeTasks[pxClusterInfo[i].usLeftBottom_x][pxClusterInfo[i].usLeftBottom_y] = 0;
        }

        usCurrentClusterX++;

        if ( usCurrentClusterX >= usClustersOnX ) {
            usCurrentClusterX = 0;
            usCurrentClusterY++;
        }
    }
}

void vApirReserveClusterResources(
        uint32_t ulCurrentCluster,
        uint32_t ulTaskResources)
{
    if ( pxClusterInfo[ ulCurrentCluster ].ulFreeResources < ulTaskResources ) {
        pxClusterInfo[ ulCurrentCluster ].ulFreeResources = 0;

    } else {
        pxClusterInfo[ ulCurrentCluster ].ulFreeResources -= ulTaskResources;
    }
}

#endif // MULTIPROCESSOR

void vApiSleepPE( void )
{
    vTaskEndScheduler();
#ifdef RTL
    UartEndSimulation();
#endif
#ifdef OVP
    MemoryWrite( WRITE_END_SIM , SIM_SUCCESS );
#endif
}
