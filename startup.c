//  PROJECT     : MAPP
//  FILE NAME   : startup.c
//  MODULE NAME : System Startup
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : processor startup, interrupts, configuration
//-----------------------------------------------------------------------------
//  PURPOSE     : Configure the system startup and the interruption vector.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------

#include "architecture_include.h"
#include "memory_macros.h"

void Nmi ( void );
void vHardProcessorFault ( void );
void vEnableNIInterruptions ( void );

#ifdef OVP
#ifndef STACK_SIZE
#define STACK_SIZE 200
#endif

static void Reset ( void );
extern void xPortPendSVHandler ( void );
extern void xPortSysTickHandler ( void );
extern void vPortSVCHandler ( void );
extern void vNetworkInterfaceHandler ( void );
static unsigned long pulMainStack[STACK_SIZE];

// The following are constructs created by the linker, indicating where the
// the "data" and "bss" segments reside in memory. The initializers for the
// for the "data" segment resides immediately following the "text" segment.
extern unsigned long _etext;
extern unsigned long _data;
extern unsigned long _edata;
extern unsigned long _bss;
extern unsigned long _ebss;

__attribute__ ( ( section("vectors") ) )
void (* const __Vectors[]) ( void ) =
{
    ( void ( * )( void ) )(( unsigned long )pulMainStack + sizeof( pulMainStack )),
    Reset,
    Nmi,
    vHardProcessorFault,
    0, // Mem Manage
    0, // Bus vHardProcessorFault
    0, // Usage fault
    0,
    0,
    0,
    0,
    vPortSVCHandler,
    0,
    0,
    xPortPendSVHandler,
    xPortSysTickHandler,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
#ifdef CORTEX_M0
    vNetworkInterfaceHandler,
#else
    0,
    0,
    0,
    vNetworkInterfaceHandler,
#endif // CORTEX_M0
    0,
    0,
    0
};

static void Reset(void)
{
    vEnableNIInterruptions();
    main();
}
#endif // OVP

void Nmi ( void )
{
#ifdef OVP
    MemoryWrite( WRITE_END_SIM , IRQ_NMI );
#else
    UartEndSimulation();
#endif // OVP
}

void vHardProcessorFault ( void )
{
#ifdef OVP
    MemoryWrite( WRITE_END_SIM , IRQ_FAULT );
#else
    UartEndSimulation();
#endif // OVP
}

void vEnableNIInterruptions ( void )
{
    __ASM volatile ("cpsie i"); //Enable interrupts
#ifdef MULTIPROCESSOR
    NVIC_SetPriority( NI_INTERRUPTION, 1 );   // Set the priority
    NVIC_ClearPendingIRQ( NI_INTERRUPTION );  // Clear the pending bit
    NVIC_EnableIRQ( NI_INTERRUPTION );        // Enable NI_INTERRUPTION
#endif
}
