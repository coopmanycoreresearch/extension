//  PROJECT     : MAPP
//  FILE NAME   : system_call.c
//  MODULE NAME : System Call Handler
//  AUTHOR      : Geancarlo Abich
//  DEVELOPERS  : Geancarlo Abich, Vitor Bandeira
//  E-mail      : abich@ieee.org, bandeira@ieee.org
//-----------------------------------------------------------------------------
//  RELEASE HISTORY
//  VERSION     DATE            DESCRIPTION
//  1.0         2018-05-01      Initial Version.
//-----------------------------------------------------------------------------
//  KEYWORDS    : SVC interruption, operating system, middleware, system abstraction
//-----------------------------------------------------------------------------
//  PURPOSE     : Handles the system calls to execute user applications and communication.
//-----------------------------------------------------------------------------
//  COMMENTS    : A)
//-----------------------------------------------------------------------------
#include "system_call.h"

#include "application_management.h"
#include "communication.h"
#include "mapping.h"
#include "memory_macros.h"
#include "resources_management.h"

/**
 * System call handler
 * We can extract the SVC number from the SVC instruction. svc_args[6]
 * points to the program counter ( the code executed just before the svc
 * call ). We need to add an offset of -2 to get to the upper byte of
 * the SVC instruction ( the immediate value ).
 *
 * svc_args[0] = r0
 * svc_args[1] = r1
 * svc_args[2] = r2
 * svc_args[3] = r3
 * svc_args[4] = r12
 * svc_args[5] = lr
 * svc_args[6] = pc
 * svc_args[6] = xpsr
 *
 **/
void vSyscallHandler( uint32_t *svc_args )
{
    uint32_t ulSystemCall ,
             ulStackedR0  ,
             ulStackedR3  ;


    vEnterCritical();
    {

        ulSystemCall = ( ( char *   ) svc_args[6] )[-2];

#ifdef MULTIPROCESSOR
        switch ( ulSystemCall ) {
            case SC_END_TASK:
                {
                    vAppEndTask();
                    break;
                }

            case SC_SEND_MSG:
                {
                    ulStackedR0  = ( ( uint32_t ) svc_args[0] );
                    ulStackedR3  = ( ( uint32_t ) svc_args[3] );
                    vApiSendMessage( ( Message* ) ulStackedR3 , ulStackedR0 - 1);
                    break;
                }

            case SC_RECV_MSG:
                {
                    ulStackedR0  = ( ( uint32_t ) svc_args[0] );
                    ulStackedR3  = ( ( uint32_t ) svc_args[3] );
                    vApiRecvMessage( ( Message* ) ulStackedR3 , ulStackedR0 - 1 );
                    break;
                }

            case SC_PRINTF:
                {
#if ( DEBUG > 1 )
                    ulStackedR0  = ( ( uint32_t ) svc_args[0] );
                    ulStackedR3  = ( ( uint32_t ) svc_args[3] );
                    vMiscPrintf( ( void * ) ulStackedR3 , ulStackedR0 );
#endif
                    break;
                }

            default:
                {
#ifdef OVP
                    MemoryWrite( WRITE_END_SIM , SYSCALL_ERROR );
#else
                    UartEndSimulation();
#endif // OVP
                    break;
                }
        }
#endif // MULTIPROCESSOR
    }
    vExitCritical();
}
